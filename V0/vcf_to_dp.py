#!/home/johan/anaconda2/bin/python

from __future__ import division
import numpy as np
import scipy as sp
from scipy.stats import *
from scipy import stats
import itertools
from scipy.special import gamma, gammaln
import math
import operator
from itertools import izip
from numpy import array, log, exp
from scipy.special import gammaln
import argparse

if __name__ == '__main__':
    description_text = 'Perform dosage calling'
    epilog_text = 'Copyright Johan Willemsen May 2016'

    parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
    parser.add_argument('-vcf', type=str,help='Input VCF')

    args = parser.parse_args()


    file_name = args.vcf.rstrip('.vcf')

    def parse_file(vcf_file):

    	f = open('NewPlusOldCalls.headed.vcf', 'r')
	    pattern = re.compile('^#')
		
		variants = []

		for row in f:
	        match = re.match(pattern, row)
	        if match:
	            continue
			
			snp = row.split('\t')
			variants += [snp[9:]]

		return variants

	variants = parse_file(args.vcf)

	def parse_SNPs(column, variants):

	    GQ, DP, RA, AA = [], [], [], [], [], [], []

	    for snp in variants:

	        try:

	        	snpd = snp[column].split(':')

	            test = snpd[0].split(':')
	            dosage = [snpd[0], snpd[2], snpd[4], snpd[6]]
	            dosages += [dosage.count('1')]
	            GQ += [float(test[1])]
	            DP += [int(test[3])]
	            RA += [int(test[4])]
	            AA += [int(test[5])]
	            locations += [[chr, pos]]
	            names += [name]            
	        except:
	            dosages += [np.nan]
	            GQ += [np.nan]
	            DP += [np.nan]
	            RA += [np.nan]
	            AA += [np.nan]
	            locations += [[chr, pos]]
	            names += [name]   
	            
	    f.close()
	    return names, locations, dosages, GQ, DP, RA, AA


    ref_count = np.loadtxt(args.r, dtype='float')
    alt_count = np.loadtxt(args.a, dtype='float')
    

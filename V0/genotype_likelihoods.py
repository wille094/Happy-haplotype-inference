#!/home/johan/anaconda2/bin/python

from __future__ import division
import numpy as np
import scipy as sp
from scipy.stats import *
from scipy import stats
import itertools
from scipy.special import gamma, gammaln
import math
import operator
from itertools import izip
from numpy import array, log, exp
from scipy.special import gammaln
import argparse

class genotype_likelihood:
    def __init__(self, counts_alleles, ploidy):

        self.GLs = self.get_GLs_bi_allelic(counts_alleles, ploidy)

    def log_factorial(self, x):
        """Returns the logarithm of x!
        Also accepts lists and NumPy arrays in place of x."""
        return np.exp(gammaln(array(x)+1))

    def multinomial(self, xs, ps):
        n = sum(xs)
        xs, ps = np.array(xs), np.array(ps)
        result = self.log_factorial(n) - sum(self.log_factorial(xs)) + sum(xs * np.log(ps))
        return np.exp(result)

    def factorial(self, u):
        if u <= 1:
            return 1
        return u*factorial(u-1)

    def multinomial_coefficient(self, a, b, c, d, pa, pb, pc,pd):
        '''get density of multinomial distribution'''
        p = a+b+c+d

        multi_prob = (self.log_factorial(p)/(self.log_factorial(a)* self.log_factorial(b) * self.log_factorial(c) * self.log_factorial(d))) * ((pa**a) * (pb**b) * (pc**c) * (pd**d))
        #multi_prob = (log_factorial(p)/( log_factorial(a)* log_factorial(b) * log_factorial(c) * log_factorial(d))) * ((pa**a) * (pb**b) * (pc**c) * (pd**d))
        return multi_prob

    def get_GLs(self, ref, alt):
        nulliplex = self.multinomial_coefficient(ref,alt,0,0,1,0,0,0)
        simplex = self.multinomial_coefficient(ref,alt,0,0,0.75,0.25,0,0)
        duplex =  self.multinomial_coefficient(ref,alt,0,0,0.5,0.5,0,0)
        triplex = self.multinomial_coefficient(ref,alt,0,0,0.25,0.75,0,0)
        quadruplex = self.multinomial_coefficient(ref,alt,0,0,0,1,0,0)
        return [nulliplex, simplex, duplex, triplex, quadruplex]

    def get_GLs_bi_allelic(self, counts, ploidy):
        '''
        Compute the GLs for all genotype classes given a ploidy level
        '''
        no_genotypes = ploidy + 1
        one_allele = 1/ploidy
        
        option_alt = np.array(np.arange(0,1,one_allele).tolist() + [1.0])
        option_ref = 1- option_alt
        options = np.vstack((option_ref, option_alt))
      
        return [self.multinomial_coefficient_arr(counts, i) for i in options.T]

    def multinomial_coefficient_arr(self, xs, ps):
        '''
        Calculate the density of a multinomial distribution over an array of alleles and frequencies. 
        '''
        
        p = sum(xs)
        xs = np.array(xs)
        ps = np.array(ps)
        
        term_b = np.product(ps**xs)
        term_a = np.product(self.log_factorial(xs))
        
        return (self.log_factorial(p) / term_a) * term_b

if __name__ == '__main__':
    description_text = 'Perform dosage calling'
    epilog_text = 'Copyright Johan Willemsen May 2016'

    parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
    parser.add_argument('-r', type=str,help='Input reference read counts')
    parser.add_argument('-a', type=str,help='Input alt read counts')
    parser.add_argument('-t', type=int, default=0, help='threshold for removing')
    parser.add_argument('-p', type=int, default=4, help='ploidy')
    parser.add_argument('-o', type=str, default='out', help='output file')

    args = parser.parse_args()

    def convert_read_counts_to_GLs(ref_count, alt_count, ploidy):
        new_array = []
        for index_row, row in enumerate(ref_count):
            # print index_row,
            new_row = []
            for index_col, RC in enumerate(row):
                AC = alt_count[index_row][index_col]
                # print RC, AC
                if RC != 'NA' and AC != 'NA':
                    GLs = genotype_likelihood([int(RC),int(AC)],ploidy).GLs
                    #print 'R', GLs
                else:
                    GLs = [1/(ploidy+1)] * (ploidy + 1)
                new_row = new_row + GLs
            new_array += [new_row]
        return np.array(new_array)

    ref_count = np.loadtxt(args.r, dtype='str')
    alt_count = np.loadtxt(args.a, dtype='str')
    
    GL = convert_read_counts_to_GLs(ref_count, alt_count, args.p)

    np.savetxt(args.o, GL)

# python genotype_likelihoods.py -r test6_ref_count.dat -a test6_alternative_count.dat
#!/home/johan/anaconda2/bin/python

from __future__ import division
import numpy as np
import scipy 
import os
import itertools
from scipy.special import gamma, gammaln
import math
from numpy import array, log, exp
import operator
import argparse

## HWE expected genotypes. 
#@profile
def factorialln(n):
    if n == 1:
        return 0
    elif n == 0:
        return 0
    else: 
        return gammaln(n + 1)
#@profile
def multinomial_coefficientln(n, counts):
    '''Computes the multinomial coefficient in log scape
    exponent of n! - sum van ! on counts. 
    '''

    return math.exp(factorialln(n) - sum(map(factorialln, counts)))
    
def product(l):
    return reduce(operator.mul, l)

# def multinomial(probs, obs):
#     '''multinomaal coefficient '''
#     return math.factorial(sum(obs)) / product(map(math.factorial, obs)) * product([math.pow(p, x) for p,x in zip(probs, obs)])

#@profile
def hwe_expectations(genotype, allele_frequencies,  ploidy):
    '''
    genotype is the counts of ABCD etc. alleles. E.G. (2,0) is 
    AA and (1,1) is AB. 
    
    ABCD = tetraploid (2,2,0,0)
    AB = diploid      (2,0,0,0)
    The allele_frequencies is the allele frequencies in a population
    or priors for each haplotype. 

    From genotype I make a numpy array row [16 haplotypes]
    So counts are: 

    '''    
    genotype_coeff = multinomial_coefficientln(ploidy, genotype)
    genotype_expected_frequency = genotype_coeff * reduce(operator.mul, [math.pow(freq, p) for freq, p in zip(allele_frequencies, genotype)])
    
    return genotype_expected_frequency

class haplotype_inference:
    '''
    We assume that the genotype array is processed and that we get a subset of this array as a nested list, where columns are the marker and columns represent the markers.

    '''
    def __init__(self, genotype_matrix, ploidy, interval_length):
        self.ploidy = ploidy
        self.interval_length = interval_length
        self.genotype_matrix = genotype_matrix
        self.m = len(self.genotype_matrix[0])

        self.pre_cached_factorials = np.array([factorialln(i) for i in range(1000)])
        self.v_index = np.vectorize(self.get_index)

        self.stopping_point = 1/ (self.m * self.ploidy)
        print self.stopping_point

    def get_index(self, x):
        return self.pre_cached_factorials[x]
    
    #@profile
    def multinomial_probabilities_array(self, n, observation_array):
        return np.exp( self.pre_cached_factorials[n] - self.v_index(observation_array).sum(axis=1))
        #return np.exp(self.pre_cached_factorials[n] - sum(map(factorialln, counts)))

    def hwe_expectations_on_array(self, allele_frequencies, observation_array):

        genotype_coefficients = self.multinomial_probabilities_array(self.ploidy, self.comp) #checked results in the same
        second_part  = np.product( np.power(allele_frequencies,  self.comp ), axis=1)
        hwe = genotype_coefficients * second_part 

        return hwe

    def remove_missing_values(self):
        '''
        Remove missing values and report used indices, the returned array is a nested list where genotypes are in rows and markers in colums (note that this is flipped to input format)
        '''
        gen_mat_proc = []
        kept_indices = []
        indices = range(self.interval_length)

        for i in range(self.m):
            new_vector = tuple([self.genotype_matrix[j][i] for j in indices])
            if 'NA' not in new_vector:
                kept_indices += [i]
                gen_mat_proc += [new_vector]
        self.genotype_matrix_without = gen_mat_proc
        self.kept_indices = kept_indices

        # print self.kept_indices
        # print self.genotype_matrix_without

    def get_alleles(self):
        '''
        Get for each marker the alleles. 
        '''
        allele = {i:[] for i in range(self.interval_length)}
        for genotype in self.genotype_matrix_without:
            for index_marker, marker in enumerate(genotype):
                allele[index_marker] = allele[index_marker] + marker

        marker_alleles = []
        for i in allele:
            marker_alleles += [list(set(allele[i]))]
        self.marker_alleles = marker_alleles
    #@profile
    def get_genotype(self, ph):
        '''
        Computes the genotypes from all phasings.
        '''
        g = np.array(ph).T
        ge = [sorted(i.tolist()) for i in g]
        return tuple(ge)
    #@profile
    def get_genotypes(self):
        '''
        Computes the unphased_genotypes, phased_genotypes and ids of every unphased and phased genotype. 
        '''
    
        self.haps = list(itertools.product(*self.marker_alleles))
        self.haps_ids = range(len(self.haps))
        self.comp = []
        self.phasings = list(itertools.combinations_with_replacement(self.haps, self.ploidy))    
        self.phasings_id = []
        for i in itertools.combinations_with_replacement(self.haps_ids,self.ploidy):
            ph = tuple([i.count(j) for j in self.haps_ids])
            self.phasings_id += [ph]
            self.comp += [list(ph)]
        self.comp = np.array(self.comp)


        self.genotypes = [self.get_genotype(i) for i in self.phasings] # list with genotypes for phasings. Each genotype might occur twice. 
        
        self.genotypes_ids = range(len(self.genotypes))

    def genotype_decomposition(self):
        '''
        Here the individual observations of unphased genotypes are decomposed into possible haplotype combis. 
        So [1,1] becomes 0.5 for 01|10 and 0.5 for 11|00. 

        returns an array with shape n indivuals * j phased genotype possibilties. (for 4 snp a 3578 x 3578, 2 snp is 35*35)
        Also here we could input the prior expectation of each haplotype, instead of using the prior of 0.5, but then we 
        use the observations from the reads. 

        This is kept the same if just inputting a array with bi_allelic snps. 

        '''
        #@profile
        def decomposition(obs_gen):
            #print obs_gen
            indices = [index for index, i in enumerate(self.genotypes) if i == obs_gen]   #get indices where obs_gen is observed
            weight = 1 / len(indices)                                                     #add weight
            col = [0] * len(self.genotypes)                                               #add weight to column
            for i in indices:                                                               
                col[i] = weight                                                             
            return col

        # normally the genotype_decomposition should be the same for all genotypes. Hence a dictionary with rows is probably the best?
        # 1. find index_obs_gen
        # 2. return row. Might not be faster 

        self.decomposed_genotype_array = []
        
        for obs_gen in self.genotype_matrix_without:
            self.decomposed_genotype_array += [decomposition(obs_gen)]
        self.decomposed_genotype_array = np.array(self.decomposed_genotype_array)

    def initialize_haplotype_priors(self, uniform=False):
        if uniform is True:
            self.haplofreq_priors = [1/len(self.haps)] * len(self.haps)
        else:
            valu = np.random.random(size=len(self.haps))
            valu = valu / valu.sum()
            self.haplofreq_priors = valu
            np.set_printoptions(precision=3)
            #print valu

    def expected_genotype_count(self):
        self.priors = self.hwe_expectations_on_array( self.haplofreq_priors, self.comp)
        #self.priors = np.array([hwe_expectations(ph, self.haplofreq_priors, self.ploidy) for ph in self.phasings_id])

    def update_priors(self, allele_freq):
        '''The only thing that changes here is the allele freq '''

        hwe1 = self.hwe_expectations_on_array(allele_freq, self.comp)
        #hwe2 = np.array([hwe_expectations(ph, allele_freq, self.ploidy) for ph in self.phasings_id])
        #print 'hwe1', hwe1[0], hwe2[0]
        return hwe1
    
    def count_haplotypes_in_genotypes(self, counts):
        '''
        Simple count function that does compute the haplotype frequency based on genotype decomposition, i.e. 
        count[0] is 00|00|00|00, hence haplotype 00 has count of 4.  
        Used in M-step to compute re-estimated haplotype frequencies. 

        counts --  Counts of each phased genotype (35 possibilities for 2-locus)

        '''

        a = counts * self.comp
        allele_counts = a.sum(axis=0)

        #allele_counts = np.array(allele_counts)
        allele_freq = allele_counts / allele_counts.sum()
        return allele_counts, allele_freq
    #@profile
    def em_algorithm(self, priors):
        
        posterior_probabilities = self.decomposed_genotype_array * priors
        #print np.product(posterior_probabilities.sum(axis=1))
        updated_GL = posterior_probabilities / posterior_probabilities.sum(axis=1).reshape(self.decomposed_genotype_array.shape[0],1)
        
        # M-step Use the expected genotype frequencies to compute the haplotype frequencies. 

        counts_genotype = updated_GL.sum(axis=0, ) #shape=(self.decomposed_genotype_array.shape[0],1))
        counts_genotype.shape = (self.decomposed_genotype_array.shape[1],1)
        
        allele_counts, allele_frequencies = self.count_haplotypes_in_genotypes(counts_genotype)
        new_priors = self.update_priors(allele_frequencies)

        return new_priors, allele_frequencies, updated_GL
    #@profile
    def iter_em(self, steps=10):
        '''
        EM algorithm to estimate haplotype frequencies. 
        '''
        
        new_priors, allele_frequencies_old, updated_GL = self.em_algorithm(self.priors)
        stop = np.abs(self.haplofreq_priors - allele_frequencies_old).mean()
        #print 'first', stop
        step = 1
        while (stop > 0.000714286) and step < steps: 
            new_priors, allele_frequencies_new, updated_GL = self.em_algorithm(new_priors)
            stop =  np.abs(allele_frequencies_new - allele_frequencies_old).mean()
            allele_frequencies_old = allele_frequencies_new
            step +=1

        # compute likelihood of The likelihood of the haplotype frequencies given the genotype counts: 

        posterior_probabilities = self.decomposed_genotype_array * new_priors
        #print 'posteriors', np.product(posterior_probabilities.sum(axis=1))
        #print allele_frequencies
        estimated_Gu = np.argmax(updated_GL, axis=1) # return the most probable assignment of haplotypes to individual

        print allele_frequencies_new
        self.estimated_freq = new_priors
        self.estimated_Gu = estimated_Gu
        self.allele_freq = allele_frequencies_new

    def add_missing_genotypes(self):

        processed_estimation = []
        for index in range(self.m):
            if index in self.kept_indices:
                index_new = self.kept_indices.index(index)
                processed_estimation += [self.estimated_Gu[index_new]]
            else:
                processed_estimation += ['NA']
        self.processed_estimation = processed_estimation

    def convert_phased_genotypes_to_string(self):

        test = [['|'.join([''.join(i) for i in j])] for j in self.phasings]

        phased_genotypes = []
        for i in self.processed_estimation:
            if i != 'NA':
                phased_genotypes += test[i]
            else:
                phased_genotypes += ['NA']
        self.phased_genotypes = phased_genotypes
        #print len(phased_genotypes)

        #self.processed_estimation 

class haplotype_wrapper(haplotype_inference):
    def __init__(self, genotype_matrix, ploidy, interval_length, skip):
        self.ploidy = ploidy
        self.genotype_matrix = genotype_matrix
        self.interval_length = interval_length
        self.no_markers = genotype_matrix.shape[0]
        self.skip = skip
    def get_sorted_list_snp_alleles(self, g):
        '''convert snps to sorted list of alleles'''
        snp1 = ['0','0','0','0']
        if g != 'NA':
            for j in range(int(g)):
                snp1[j] = '1'
            return sorted(snp1)
        else:
            return 'NA'

    def split_hap(self, g):
        ''' split input unsorted alleles to list '''
        if g != 'NA':
            snp1 = g.split('|')
            #print snp1, g
            return sorted(snp1)
        else:
            return 'NA'
    
    def convert_array(self, gen_mat, split=False):
        ''' convert genotype array '''
        if split is False:
            return [map(self.get_sorted_list_snp_alleles,i) for i in gen_mat.tolist()]
        else:
            return [map(self.split_hap,i) for i in gen_mat.tolist()]

    def iterative_haplotype(self, split=True):
        self.new_genotype_matrix = []
        t_file = open(args.o.rstrip('.txt') + '.hapfreq.txt', 'wb')
        cnt = len(range(0, self.no_markers-self.interval_length, self.skip))

        #self.marker_alleles = [[0,1],[0,1]]


        for interval in range(0, self.no_markers-self.interval_length+1, self.skip):

            print interval,
            indices = range(interval,interval+self.interval_length)
            subset_mat = self.genotype_matrix[indices,:]
            #print subset_mat.shape
            converted_test = self.convert_array(subset_mat, split=True)
            
            haplotyping = haplotype_inference(converted_test, self.ploidy,self.interval_length)
            haplotyping.remove_missing_values()
            # Todo: Speed up computation in the genotype decomposition, as that takes ages. 
            haplotyping.get_alleles()
            haplotyping.get_genotypes()
            haplotyping.genotype_decomposition()
            haplotyping.initialize_haplotype_priors()
            haplotyping.expected_genotype_count()
            haplotyping.iter_em()
            haplotyping.add_missing_genotypes()
            haplotyping.convert_phased_genotypes_to_string()
            #print haplotyping.phasings
            self.new_genotype_matrix += [haplotyping.phased_genotypes]
            #print haplotyping.haps
            t = [''.join(i) for i in haplotyping.haps] + [str(i) for i in haplotyping.allele_freq]
            t_file.write('\t'.join(t) + '\n')
        t_file.close()
        self.new_genotype_matrix = np.array(self.new_genotype_matrix)

def get_sorted_list_snp_alleles(g):
    snp1 = ['0','0','0','0']
    if g != 'NA':
        for j in range(int(g)):
            snp1[j] = '1'
        return sorted(snp1)
    else:
        return 'NA'
    
if __name__ == '__main__':

    description_text = 'Haplotype inference using the EM algorithm.'
    epilog_text = 'Copyright Johan Willemsen May 2016'

    parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
    parser.add_argument('-i', type=str,help='Input genotype matrix file, if - then read from stdin')
    parser.add_argument('-o', type=str, default='-', help='Output file name, if nothing is specified then put to stdout' )
    parser.add_argument('-interval_length', type=int, default=4, help='Length of the interval used for haplotyping, default is 4')
    parser.add_argument('-ploidy', type=int, default=4, help='The ploidy level')
    parser.add_argument('-skip', type=int, default=1, help='Number of markers to skip, normally over a sliding window of 1 snp')
    parser.add_argument('-mode', type=int, default=1, help='Mode to compute the genotype decomposition only once')

    args = parser.parse_args()

    #check if SNP is inputed, do conversion then!

    ploidy = int(args.ploidy)
    interval_length = int(args.interval_length)
    skip = int(args.skip)
    genotype_matrix = np.loadtxt(args.i, dtype='str') # markers > genotyeps
    hap = haplotype_wrapper(genotype_matrix, ploidy, args.interval_length, skip)
    hap.iterative_haplotype()

    np.savetxt(args.o, hap.new_genotype_matrix, fmt='%s', delimiter='\t')




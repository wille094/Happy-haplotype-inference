#!/home/johan/anaconda2/bin/python

from __future__ import division
import numpy as np
import scipy 
import os
import itertools
from scipy.special import gamma, gammaln
import math
from numpy import array, log, exp
import operator
import argparse

def factorialln(n):
    if n == 1:
        return 0
    elif n == 0:
        return 0
    else: 
        return gammaln(n + 1)

def multinomial_coefficientln(n, counts):
    '''Computes the multinomial coefficient in log scape
    exponent of n! - sum van ! on counts. 
    '''

    return math.exp(factorialln(n) - sum(map(factorialln, counts)))
    
def product(l):
    return reduce(operator.mul, l)

def hwe_expectations(genotype, allele_frequencies,  ploidy):
    '''
    genotype is the counts of ABCD etc. alleles. E.G. (2,0) is 
    AA and (1,1) is AB. 
    
    ABCD = tetraploid (2,2,0,0)
    AB = diploid      (2,0,0,0)
    The allele_frequencies is the allele frequencies in a population
    or priors for each haplotype. 

    From genotype I make a numpy array row [16 haplotypes]
    So counts are: 

    '''    
    genotype_coeff = multinomial_coefficientln(ploidy, genotype)
    genotype_expected_frequency = genotype_coeff * reduce(operator.mul, [math.pow(freq, p) for freq, p in zip(allele_frequencies, genotype)])
    
    return genotype_expected_frequency

class haplotype_inference:
    '''
    We assume that the genotype array is processed and that we get a subset of this array as a nested list, where columns are the marker and columns represent the markers.

    '''
    def __init__(self, genotype_matrix, ploidy, threshold=0):
        self.ploidy = ploidy
        self.genotype_matrix = genotype_matrix # [alleles] * markers * genotypes list

        # [0,0,0,1] [0,0,0,1] < G1
        # [N,N,N,N] [0,0,0,1] < G2

        self.m = len(self.genotype_matrix[0])   # number of markers
        self.n = len(self.genotype_matrix)      # Number of genotypes

        self.pre_cached_factorials = np.array([factorialln(i) for i in range(1000)])
        self.v_index = np.vectorize(self.get_index)
        self.p_threshold = threshold
        self.stopping_point = 1/ (self.m * self.ploidy)

        #list to record all values of every cycle!
        self.estimated_Gu_list = []
        self.loglikelihood_list = []
        self.estimated_freq_list = []
        self.allele_freq_list = []
        self.GL_list = []
        self.allele_counts_list = []
        self.LR_list = []

    def get_index(self, x):
        return self.pre_cached_factorials[x]
    
    def multinomial_probabilities_array(self, n, observation_array):
        return np.exp( self.pre_cached_factorials[n] - self.v_index(observation_array).sum(axis=1))
        #return np.exp(self.pre_cached_factorials[n] - sum(map(factorialln, counts)))

    def hwe_expectations_on_array(self, allele_frequencies, observation_array):

        genotype_coefficients = self.multinomial_probabilities_array(self.ploidy, self.comp) #checked results in the same
        second_part  = np.product( np.power(allele_frequencies,  self.comp ), axis=1)
        hwe = genotype_coefficients * second_part 
        return hwe

    def get_alleles(self):
        '''
        Get for each marker the alleles. It does support multi-allelic markers, but not the value 'NA' 
        '''
        allele = {i:[] for i in range(self.m)}
        for genotype in self.genotype_matrix:
            for index_marker, marker in enumerate(genotype):
                #print marker
                if 'NA' not in marker:
                    allele[index_marker] = allele[index_marker] + marker

        marker_alleles = []
        for i in allele:
            marker_alleles += [list(set(allele[i]))]
        self.marker_alleles = marker_alleles    #returns list of all unique alleles for each marker [[00,01] , [00,10,11,01]]

    def get_genotype(self, ph):
        '''
        Computes the genotypes from all phasings.
        '''
        g = np.array(ph).T
        ge = [sorted(i.tolist()) for i in g]
        return tuple(ge)

    def get_genotypes_prob(self, haps=None, prune=False):
        '''
        Computes the unphased_genotypes, phased_genotypes and ids of every unphased and phased genotype. 
        '''
        if haps == None:
            self.haps = list(itertools.product(*self.marker_alleles))       # compute all haplotypes
            # print self.haps
        else:
            self.haps = haps

        # Here the list with unique genotypes is created TODO: Works only for two-snp analysis. 
        a = [list(i) for i in list(itertools.combinations_with_replacement(['0','1'], 4))]
        b = [list(i) for i in list(itertools.combinations_with_replacement(['0','1'], 4))]
        genotypes = list(itertools.product(a,b))

        # Here the phasings are created. 
        self.haps_ids = range(len(self.haps))                           # Give all haplotypes an ID
        self.comp = []      
        self.phasings = list(itertools.combinations_with_replacement(self.haps, self.ploidy)) # compute the phasings (combinations of haps)
        
        # Make haplotype indices list for complete list of haplotypes

        for i in itertools.combinations_with_replacement(self.haps_ids,self.ploidy):
            ph = tuple([i.count(j) for j in self.haps_ids])
            self.comp += [list(ph)]
        self.comp = np.array(self.comp)

        # get genotypes for phasing. # this takes a while for large samples. 
        self.genotypes = [self.get_genotype(i) for i in self.phasings] # list with genotypes for phasings. Each genotype might occur twice. 
        #print 'genotypes from phasings', self.genotypes
        # Convert the genotypes list to a dictionary of indices. 


        unique_genotypes = [] # 25 in case of tetraploid two-locus, with 35 phasings. 
        genotype_indices = [] 

        # Rewrite of the above stated genotype so that a phasing can have more than one genotype!
        # print 'lengenotypes', len(self.genotypes)

        for gen_index, gen in enumerate(self.genotypes):
            if gen not in unique_genotypes:
                unique_genotypes += [gen]
                # indices = [index for index, i in enumerate(self.genotypes) if i == gen] 
                #print 'xx'
                # genotype_indices += [indices]
                genotype_indices += [[gen_index]]
            else:
                NR_gen_index = unique_genotypes.index(gen)
                genotype_indices[NR_gen_index] += [gen_index]

        genotype_indices = []
        unique_genotypes = genotypes
        for gen_index, gen in enumerate(genotypes):
            indices = [index for index, i in enumerate(self.genotypes) if i == gen] 
            genotype_indices += [indices]

        # print unique_genotypes
        self.genotypes_unique = unique_genotypes
        self.genotypes_indices = genotype_indices

    def get_genotypes(self, haps=None, prune=False):
        '''
        Computes the unphased_genotypes, phased_genotypes and ids of every unphased and phased genotype. 
        '''
        if haps == None:
            self.haps = list(itertools.product(*self.marker_alleles))       # compute all haplotypes
            # print self.haps
        else:
            self.haps = haps

        self.haps_ids = range(len(self.haps))                           # Give all haplotypes an ID
        self.comp = []      
        self.phasings = list(itertools.combinations_with_replacement(self.haps, self.ploidy)) # compute the phasings (combinations of haps)
        
        # Make haplotype indices list for complete list of haplotypes
        for i in itertools.combinations_with_replacement(self.haps_ids,self.ploidy):
            ph = tuple([i.count(j) for j in self.haps_ids])
            self.comp += [list(ph)]
        self.comp = np.array(self.comp)

        # get genotypes for phasing. # this takes a while for large samples. 
        self.genotypes = [self.get_genotype(i) for i in self.phasings] # list with genotypes for phasings. Each genotype might occur twice. 
        
        # Convert the genotypes list to a dictionary of indices. 

        unique_genotypes = [] # 25 in case of tetraploid two-locus, with 35 phasings. 
        genotype_indices = [] 

        # Rewrite of the above stated genotype so that a phasing can have more than one genotype!
        for gen_index, gen in enumerate(self.genotypes):
            if gen not in unique_genotypes:
                unique_genotypes += [gen]
                genotype_indices += [[gen_index]]
            else:
                NR_gen_index = unique_genotypes.index(gen)
                genotype_indices[NR_gen_index] += [gen_index]

        #print 'y'
        self.genotypes_unique = unique_genotypes
        self.genotypes_indices = genotype_indices

    def impute_NA(self, obs_gen):
        ind_NA = [j for j in range(len(obs_gen)) if obs_gen[j] == na]

        temp_obs = []

        for k in range(len(obs_gen)):           # for all indices
            if k not in ind_NA:
                 temp_obs += [[tuple(obs_gen[k])]]
            else:
                 possible_alleles = self.marker_alleles[k]
                 possible_scores = list(itertools.combinations_with_replacement(possible_alleles,self.ploidy))
                 temp_obs += [possible_scores]
        indices = []

        for l in itertools.product(*temp_obs):
            potential_genotype = tuple([sorted(list(m)) for m in l])
            #print potential_genotype
            pot_ind = self.genotypes_unique.index(potential_genotype)
            indices += [pot_ind]
            
        indices = sorted(list(set(indices)))
        return indices

    def decomposition_genotype_probabilities(self, genotype_probabilities):
        '''The observation will be [0,0,0,0,1], [0,0,0,0,1] of length interval_length or a value between 0 and 1.
        We do not need to impute NA's, this can be done during conversion by assigning a equal probability to genotypes. 
        A genotype with length of 1 is just a SNP calling which is subsequently updated using HWE. 

        N x N SxN
        '''

        decomposition_array = []

        # Generate the decomposed array, the decomposed array has length of phasings, and depth of observations, so 100, 35. 
        for index, obs in enumerate(genotype_probabilities):
            # print index, obs,
            # A observation has equal length to self.genotypes_indices. 
            indices = self.genotypes_indices[index]
            # print obs/len(indices), self.genotypes_indices[index]

            weight = obs / len(indices)        # add weight proportional to genotype probaliities. 
            col = [0] * len(self.phasings)     # add weight to column
            for i in indices:                                                               
                col[i] = weight 
            decomposition_array += [col]
        decomposition_array = np.array(decomposition_array)

        col = decomposition_array.sum(axis=0)   # each genotype gets a score, which together will add to 1. 
        return col

    def genotype_decomposition_probability(self):
        self.decomposed_genotype_array = []
        for obs_gen_index, obs_gen in enumerate(self.genotype_matrix):
            # print 'obs', obs_gen, sum(obs_gen)
            self.decomposed_genotype_array += [self.decomposition_genotype_probabilities(obs_gen)]
        self.decomposed_genotype_array = np.array(self.decomposed_genotype_array)
        # print self.decomposed_genotype_array.shape

    def decomposition(self,obs_gen):
        # The observation can be as follows : ([0,0,0,0],[0,0,0,1]) or with NA ([0,0,0,0],[N,N,N,N])
        #print 'obsgen', obs_gen
        #print 'start decomp'
        na = ['NA'] * self.ploidy

        if na in obs_gen: # check if missing value is in there
            
            ind_NA = [j for j in range(len(obs_gen)) if obs_gen[j] == na]
            # get all possible NA's. 
            temp_obs = []

            for k in range(len(obs_gen)):           # for all indices
                if k not in ind_NA:
                     temp_obs += [[tuple(obs_gen[k])]]
                else:
                     possible_alleles = self.marker_alleles[k]
                     possible_scores = list(itertools.combinations_with_replacement(possible_alleles,self.ploidy))
                     temp_obs += [possible_scores]
            indices = []

            for l in itertools.product(*temp_obs):
                potential_genotype = tuple([sorted(list(m)) for m in l])
                #print potential_genotype
                pot_ind = self.genotypes_unique.index(potential_genotype)
                indices += [pot_ind]
                
            indices = sorted(list(set(indices)))
        else:
            
            ind = self.genotypes_unique.index(obs_gen)
            indices = self.genotypes_indices[ind]

        #print indices   
        weight = 1 / len(indices)                                                     #add weight
        col = [0] * len(self.phasings)                                               #add weight to column
        for i in indices:                                                               
            col[i] = weight                                                             
        return col

    def genotype_decomposition(self):
        '''
        Here the individual observations of unphased genotypes are decomposed into possible haplotype combis. 
        So [1,1] becomes 0.5 for 01|10 and 0.5 for 11|00. 

        returns an array with shape n indivuals * j phased genotype possibilties. (for 4 snp a 3578 x 3578, 2 snp is 35*35)
        Also here we could input the prior expectation of each haplotype, instead of using the prior of 0.5, but then we 
        use the observations from the reads.
        '''
        # normally the genotype_decomposition should be the same for all genotypes. Hence a dictionary with rows is probably the best?
        # 1. find index_obs_gen
        # 2. return row. Might not be faster 

        self.decomposed_genotype_array = []
        for obs_gen_index, obs_gen in enumerate(self.genotype_matrix):
            #print obs_gen, obs_gen_index
            self.decomposed_genotype_array += [self.decomposition(obs_gen)]
        self.decomposed_genotype_array = np.array(self.decomposed_genotype_array)
        
    def initialize_haplotype_priors(self, uniform=False):
        if uniform is True:
            self.haplofreq_priors = [1/len(self.haps)] * len(self.haps)
        else:
            valu = np.random.random(size=len(self.haps))
            valu = valu / valu.sum()
            self.haplofreq_priors = valu
            np.set_printoptions(precision=3)
            #print valu

    def expected_genotype_count(self):
        self.priors = self.hwe_expectations_on_array( self.haplofreq_priors, self.comp)
        #self.priors = np.array([hwe_expectations(ph, self.haplofreq_priors, self.ploidy) for ph in self.phasings_id])
        #print self.priors

    def update_priors(self, allele_freq):
        '''The only thing that changes here is the allele freq '''

        hwe1 = self.hwe_expectations_on_array(allele_freq, self.comp)
        #hwe2 = np.array([hwe_expectations(ph, allele_freq, self.ploidy) for ph in self.phasings_id])
        #print 'hwe1', hwe1[0], hwe2[0]
        return hwe1
    
    def count_haplotypes_in_genotypes(self, counts):
        '''
        Simple count function that does compute the haplotype frequency based on genotype decomposition, i.e. 
        count[0] is 00|00|00|00, hence haplotype 00 has count of 4.  
        Used in M-step to compute re-estimated haplotype frequencies. 

        counts --  Counts of each phased genotype (35 possibilities for 2-locus)

        '''

        a = counts * self.comp
        allele_counts = a.sum(axis=0)

        #allele_counts = np.array(allele_counts)
        allele_freq = allele_counts / allele_counts.sum()
        return allele_counts, allele_freq

    def calculate_haplotype_frequency_on_decomposed_array(self):
        # simple count function that compute the haplotype frequency only for 
        # The decomposed array has 35x
        
        new_decomp_array = np.copy(self.decomposed_genotype_array)
        new_decomp_array[np.where(self.decomposed_genotype_array != 1)] = 0.0
        
        counts_genotype = new_decomp_array.sum(axis=0, ) #shape=(self.decomposed_genotype_array.shape[0],1))
        counts_genotype.shape = (new_decomp_array.shape[1],1)
        
        a = counts_genotype * self.comp
        allele_counts = a.sum(axis=0)
        allele_freq = allele_counts / allele_counts.sum()

        return allele_freq, allele_counts

    def composite_haplotyping_method(self):

        # first calculate allele frequency
        allele_freq, allele_counts = self.calculate_haplotype_frequency_on_decomposed_array() # calculate the allele freq
        new_priors = self.update_priors(allele_freq)
        # then update genotype counts?

        posterior_probabilities = self.decomposed_genotype_array * new_priors
        updated_GL = posterior_probabilities / posterior_probabilities.sum(axis=1).reshape(self.decomposed_genotype_array.shape[0],1)
        
        return new_priors, allele_freq, updated_GL, allele_counts


    def em_algorithm(self, priors):
        
        posterior_probabilities = self.decomposed_genotype_array * priors
        #print np.product(posterior_probabilities.sum(axis=1))
        updated_GL = posterior_probabilities / posterior_probabilities.sum(axis=1).reshape(self.decomposed_genotype_array.shape[0],1)
        
        # M-step Use the expected genotype frequencies to compute the haplotype frequencies. 

        counts_genotype = updated_GL.sum(axis=0, ) #shape=(self.decomposed_genotype_array.shape[0],1))
        counts_genotype.shape = (self.decomposed_genotype_array.shape[1],1)
        
        allele_counts, allele_frequencies = self.count_haplotypes_in_genotypes(counts_genotype)
        new_priors = self.update_priors(allele_frequencies)

        return new_priors, allele_frequencies, updated_GL, allele_counts


    def iter_em(self, steps=50):
        '''
        EM algorithm to estimate haplotype frequencies. 
        '''
        m=True
        if m == True:

            new_priors, allele_frequencies_old, updated_GL, allele_counts = self.em_algorithm(self.priors)
            allele_frequencies_new = True
            stop = np.abs(self.haplofreq_priors - allele_frequencies_old).mean()
            #print 'first', stop
            
            # Test to compute phasings based on unambigous genotypes

            step = 1
            while (stop > 0.000714286) and step < steps:
                
                new_priors, allele_frequencies_new, updated_GL, allele_counts = self.em_algorithm(new_priors)
                stop =  np.abs(allele_frequencies_new - allele_frequencies_old).mean()
                allele_frequencies_old = allele_frequencies_new
                step +=1

            # compute likelihood of The likelihood of the haplotype frequencies given the genotype counts: 
        else:

            new_priors, allele_frequencies_new, updated_GL, allele_counts = self.composite_haplotyping_method()
    
        posterior_probabilities = self.decomposed_genotype_array * new_priors
        
        self.allele_counts = allele_counts

        # Is this the stupid log-likelihood 
        posterior_prob = np.product(posterior_probabilities.sum(axis=1))
        estimated_Gu = np.argmax(updated_GL, axis=1) # return the most probable assignment of haplotypes to individual
        
        # Get likelihood ratio

        LRs = []
        for i in updated_GL:
            temp_i = sorted(i)
            # print temp_i, temp_i[-1], temp_i[1], 
            try:
                lr = temp_i[-1] / (temp_i[-2] + 0.0000000000000000000000000000001)
                if lr > 50000:
                    lr = 50000
            except:
                lr = 50000

            LRs += [lr]

        self.estimated_freq = new_priors
        self.estimated_Gu = estimated_Gu
        if allele_frequencies_new is not True:
            self.allele_freq = allele_frequencies_new
        else:
            self.allele_freq = allele_frequencies_old

        self.GL_list += [updated_GL.max(axis=1)]
        self.estimated_freq_list += [self.estimated_freq]
        self.estimated_Gu_list += [self.estimated_Gu]
        self.allele_freq_list += [self.allele_freq]
        self.loglikelihood_list += [np.log10(posterior_prob)]
        self.allele_counts_list += [self.allele_counts]
        self.LR_list += [LRs]

    def create_haplotypes(self, indices_modus=None):

        if indices_modus == None:
            indices_modus = self.estimated_Gu

        test = [['|'.join([''.join(i) for i in j])] for j in self.phasings]
        phased_genotypes = []
        phased_real = []
        phased_indices = []

        for i in indices_modus:
            if i != 'NA':
                # print i
                phased_genotypes += test[int(i)]
                phased_real += [np.array(self.phasings[int(i)])]
                phased_indices += [str(i)]
            else:
                phased_genotypes += ['NA']
                phased_real += ['NA']
                phased_indices += ['NA']
        self.phased_genotypes = phased_genotypes
        self.phased_real = phased_real
        self.phased_indices = phased_indices
                
    def select_best_run(self):
    
        index = np.argmax(np.array(self.loglikelihood_list))
        #print index, np.array(self.loglikelihood_list).max()
        #print index
        self.estimated_freq = self.estimated_freq_list[index]
        self.discard_indices = np.where((self.GL_list[index] < self.p_threshold))[0]
        self.estimated_Gu = self.estimated_Gu_list[index]
        self.allele_freq = self.allele_freq_list[index]
        self.allele_counts = self.allele_counts_list[index]

        # Actual allele frequency based on estimated GLs, as the frequency estimate might be zero, still an unambigous individual can be inserted
        # print self.haps
        # print self.allele_freq
        real_counts = []
        for i in self.estimated_Gu:
            real_counts += [[self.phasings[i].count(H) for H in self.haps]]
        
        self.real_counts = np.array(real_counts).sum(axis=0)
        
        #print 'real_counts', self.real_counts, sum(self.real_counts)

        self.real_freq = self.real_counts / self.real_counts.sum()
        # print 'real_freq', self.real_freq, self.real_freq.sum()

        #print 'no runs', len(self.loglikelihood_list)

        self.GLs = self.GL_list[index]
        self.LRs = self.LR_list[index]



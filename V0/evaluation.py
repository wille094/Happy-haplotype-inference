from __future__ import division
import numpy as np
import itertools



def compare_haplotypes(obs_haplotypes, converted_haplotypes, length_haplotypes=2):
    '''compare haplotypes and report accuracy'''
    m, n = obs_haplotypes.shape
    x = len(list(itertools.combinations(range(length_haplotypes), 2)))
    #print m, n, x
    score_array = np.zeros((m,n), dtype='float')
    switch_array = np.zeros((m,n), dtype='float')
    switch_array.fill(np.nan)
    pairwise_array = np.zeros((m,n),  dtype='float')
    
    pair_wise_dict_array = np.zeros((m,n,x), dtype='float')
    # print pair_wise_dict_array.shape
    
    # For all markers?
    for i in range(0, m):
        # For all cultivars
        for j in range(0, n):

            if 'N' in obs_haplotypes[i][j]:
                score_array[i][j] = np.nan
                pairwise_array[i][j] = np.nan
                pair_wise_dict_array[i][j].fill(np.nan)
                continue

            observed_block =  parse_haplotype_observation(obs_haplotypes[i][j])
            theoretical_block = parse_haplotype_observation(converted_haplotypes[i][j])
            exact_score = get_number_of_correct_haps(observed_block, theoretical_block)
            pairwise_accuracy, pairwise_dict, pairwise_list =  pair_wise_accuracy(observed_block, theoretical_block)
            try:
                no_switches, where_switch = get_switch_error(theoretical_block, observed_block)
            except:
                no_switches = np.nan

            score_array[i][j] = exact_score
            pairwise_array[i][j] = pairwise_accuracy
            switch_array[i][j] = no_switches
            for index, k in enumerate(pairwise_list[0]):
                #print k
                pair_wise_dict_array[i][j][index] = k
        
    return score_array, pairwise_array, pair_wise_dict_array, switch_array
            
def parse_haplotype_observation(obs):
    haps = obs.split('|')
    haps = [[int(i) for i in j] for j in haps]
    return np.array(haps)


def get_number_of_correct_haps(haps, selection):
    score = 0
    sel = list(selection.tolist()[:])
    # print sel
    for hap in haps.tolist():
        if hap in sel:
            score += 1
            sel.pop(sel.index(hap))
    return score

def pair_wise_accuracy(blocka, blockb):
    '''
    Return pairwise accuracy estimate, of all possible pairs. 
    so 1-2, 1-3, But also dictionary of accuracy along intervals. 
    '''
    
    ploidy, length = blocka.shape
    options = list(itertools.combinations(range(length), 2))

    pairwise_dict = {}
    if options != []:
        pairwise_dict = {}
        pairwise_list = []
        score = []
        for i in options:
            
            haps = blocka[:,i]
            selection = blockb[:,i]

            score += [get_number_of_correct_haps(haps, selection)]
            pairwise_dict[i] = score
            pairwise_list += [score]
            
        #print len(pairwise_list)
        return sum(score)/(len(score)*ploidy), pairwise_dict, pairwise_list

    else:
        return 'NA', pairwise_dict, 'NA'

def convert_haplotypes_to_links(haplotypes, skip, haplotype_length, ploidy):
    ''' Parser for haplotypes in array, convert to 00|xx'''
    m, n = haplotypes.shape
    
    eval_haplotypes = []
    for start in range(0, m, skip):
        hap_string = []
        haps = haplotypes[start: start+haplotype_length]
        
        for H in range(0,n,ploidy):
            hap_string += ['|'.join([''.join([str(j) for j in i]) for i in haps.T[H:H+ploidy]])]
        eval_haplotypes  += [hap_string]
        
    return np.array(eval_haplotypes)    

def get_switch_error(haplotypes, infered_haplotype):
    no_switches = 0
    where_switch = []
    
    new_infered = np.copy(infered_haplotype)
    
    for index, i in enumerate(range(2,haplotypes.shape[1])):
        temp_hap = haplotypes[:,:i]
        temp_inf = new_infered[:,:i]
        
        score, wrong = get_number_of_correct_haps2(temp_inf, temp_hap)
        if score < 4:
            
            no_switches += 1
            where_switch += [i]
            
        combinations = list(itertools.permutations(wrong, len(wrong)))
        
        wait_hap = np.copy(new_infered)
        
        while score != 4 or combinations != [()]:
            
            u = combinations.pop() #(0,1, 1, 0)
            for index_k, k in enumerate(u):
                from_H = wrong[index_k]
                wait_hap[from_H,:i-1] = temp_inf[k,:-1]
            
            score, wrong2 = get_number_of_correct_haps2(wait_hap[:,:i], temp_hap)

            if score == 4:
                break
        
        new_infered = wait_hap
    return no_switches, where_switch

def get_number_of_correct_haps2(haps, selection):
    score = 0
    sel = list(selection.tolist()[:])
    wrong = []
    for index, hap in enumerate(haps.tolist()):
        if hap in sel:
            score += 1
            sel.pop(sel.index(hap))
        else:
            wrong += [index]
    return score, wrong

def convert_ref_alt_to_major_minor(dosages, ploidy, freq=None):
    new_dosages = np.copy(dosages)
    
    if freq is None:
        alelle_freq = dosages.sum(axis=1) / (dosages.shape[1]*ploidy)
    else:
        alelle_freq = freq
    
    for index, i in enumerate(alelle_freq):
        if i > 0.5:
            if freq is None:
                new_dosages[index] = ploidy - new_dosages[index]
            else:
                new_dosages[index] = (new_dosages[index] - 1)**2
                #print i, new_dosages[index].mean()
    return new_dosages

def get_sorted_list_snp_alleles(ploidy, g):
    snp1 = ['0'] * ploidy
    if g != 'NA':
        for j in range(int(g)):
            snp1[j] = '1'
        return '|'.join(sorted(snp1))
    else:
        return 'NA'



# Implementation

Author: Johan Willemsen
version: v.09 (may 2020)
Description: Un-optimised version of polyhappy for haplotype inference of unphased bi-allelic SNPs. Allows to compute longer haplotypes. 

# Installation

In principle the installation via conda should work fine.
```
conda create -n happy-hap python=2.7.* 
conda activate happy-hap
pip install scipy numpy

```

# Haplotype inference in polyploid crops

The following procedure can be performed to obtain haplotypes for polyploids (=tetraploid) crops. 
1. In the first step we estimate pairwise phasings with the EM-algorithm. Note that for n snps there are n(n-1)/2 pairwise combinations, which does not scale. 
2. The second step uses an iterative algorithm that glues pairwise phasings together to a full length haplotype. 

With the first step it is also possible to calculate longer haplotypes, although computational requirements will go boom!
First we start off with an reconstruction of pairwise phasings and follow this by the joining of these phases. 

```bash
python wrapper.py -i ./Example/gwd.all.dat -o gwd_hap -ploidy 4 -join -joining_steps 100
```

To obtain haplotypes from blocks you can also specify the -block_list parameter. The block_list parameter can link to a file that links markers to blocks. 
```
block1
block1
block2
block2
```
In the above example the first two markers in gwd.all.dat will be placed into a single block. 

```bash
python wrapper.py -i ./Example/gwd.all.dat -o gwd_hap -ploidy 4 -join -joining_steps 100 -split_list ./Example/block_list.txt
```


The `-X` option specifies the distance in SNPs, for which pairwise phasings are estimated. With `-X` of 100, this would mean that only pairwise phasings between SNP1.. SNP2, ..., and SNP100 are computed, but not for SNP1 and SNP101, SNP102. (not for use!!!)

The `-join` option allows the computation of longer haplotypes. If not specified only pairwise phasings will be computed (see *.blk file for these pairwise phasings). 

**Other arguments**:

1. `-steps`: Number of iterations for EM
2. `-sampling_no`: Number of samples
3. `-sampling_proportion`: proportion of samples that are used for repetition 
4.  `-em`: Only compute the pairwise phasings
5. `-joining_steps`: Number of iterations for the joining algorithm.

# Output

At the moment a lot of output files are created. The most important ones are the final output were the full-length reconstructed haplotypes are present. 

1. {prefix}.long : haplotypes. Each row represents a haplotype, so four rows for a cultivar. 
2. {prefix}.long.ratio : Confidence ratio based on number of iterations. If 10000, then all iterations result in the same haplotype. Can be used for selecting high-quality solutions
3. {prefix}.unique: All unique haplotypes and counts. 

3. {prefix}.blk : All pairwise phasings
4. {prefix}.frq : Estimated frequencies of pairwise phasings
5. {prefix}.gls : genotype likelihoods of pairwise phasings
6. {prefix}.rep : Results from repeats of subsampling for em
7. {prefix}.lr  : Likelihood ratio for 1 and 2nd best solution. 

Using the example data we find 28 haplotypes, from which only a few are seen frequently. Haplotype inference has difficulty with estimation of rare alleles, which is just how it is. 

# TWO-step

It is possible run the two steps seperately. 
1. First compute the pairwise phasings. 
2. Then you specify the {prefix}.blk file as input. The output file with only the blocks can be used in the joining step by specifying the `iblock` parameter:

```bash
python wrapper.py -i ./Example/gwd.all.dat -o haplotype_matrix -ploidy 4 -X 100 -em -sampling_no 5
python wrapper.py -i ./Example/gwd.all.dat -iblock haplotype_matrix.blk -o haplotype_matrix.txt.long -ploidy 4 -join -X 100 
```



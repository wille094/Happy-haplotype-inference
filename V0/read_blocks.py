from __future__ import division
import numpy as np

def read_bins_file(file_name):

    bins_file = open(file_name, 'r')
    
    blocks, haps = [], []
    block, hap = [], []
    dosages = []
    
    for i in bins_file:
        cnt =  i.strip().split('\t')
        # TODO: Rewrite to make universal, now only values potvar and SNP are allowed
        if 'PotVar' in cnt[0] or 'SNP' in cnt[0]:
            block += [cnt[0]]
            hap += [cnt[1:]]
            if 'NA' not in cnt[0]:
                dosages  += [cnt[1:].count('1')]
                # rint cnt[0], cnt[1:].count('1'), cnt[1:]
            else:
                dosages += ['NA']
        else:
            if block != []:
                blocks += [block]
                haps += [np.array(hap).T.tolist()]   
            block, hap = [], []
    
    if hap != []:
        blocks += [block]
        haps += [np.array(hap).T.tolist()]   
    
    return blocks, haps, dosages
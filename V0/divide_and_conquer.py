from __future__ import division
import numpy as np
import itertools
# import networkx as nx
# import matplotlib.pyplot as plt
import random
import argparse
import time
import datetime

def wrap_join_hybrid(alleles, haplotypes, block_file, ploidy, X, output, reflibrary, iter_n=100):

    allele_matrix = alleles.T

    # get haplotype blocks: Here the vectors containing the allele dosages are inserted. 
    if haplotypes == []:
        for genotype in allele_matrix:
            gen_hap = []
            for i in genotype:
                if i == 'NA':
                    gen_hap += [[['N']] * ploidy]
                else:
                    t = [['0']] * ploidy
                    #print t
                    for j in range(int(i)):
                        #print j,
                        t[j] = ['1']
                    gen_hap += [t] 
            haplotypes += [gen_hap]
    # print haplotypes

    assert haplotypes != []

    no_cultivars = allele_matrix.shape[0]
    print datetime.datetime.now().strftime("%d-%m-%y\t%H:%M:%S\t"),
   
    print 'Join %i genotypes...' %(no_cultivars)
    ratios = []
    final_haps = []

    all_haplotypes_frequency = {}

    for index in xrange(no_cultivars):
        print datetime.datetime.now().strftime("\t%H:%M:%S\t"), 'Genotype %i' %(index)
        
        # get dosages of cultivar [index] and determine which markers miss information
        dosages = allele_matrix[index]
        missing = [[k for k, i in enumerate(dosages) if i == 'NA']]
        blocks_genotype = haplotypes[index]
        
        # open the blk file reconstructed with the function to generate pairwise phasings. 
        phasings_nodes = {}
        g = open(block_file, 'r')
        for line in g:
            combi = tuple([int(j) for j in line.split('\t')[0:2]])
            bl = line.strip().split('\t')[index+3]
            if bl == 'N|A':
                # print bl
                continue
            phasings_nodes[combi] = split_hap(line.strip().split('\t')[index+3])
        g.close()

        assert phasings_nodes != []
        # perform the haplotype reconstruction
        hap = hybrid_joining(phasings_nodes, blocks_genotype, dosages, missing, reflibrary, ploidy, X=X)
        
        unique_phasings = []
        unique_phasings_count = []
        all_haps = {}

        # We iterate a 100 times over the joining algorithm, which allows to compute confidence in our phasings.     

        for i in range(iter_n):
        
            hap.join_random(random_option=True)
            haps = np.array(hap.haplotypes).tolist()
            
            scores = [get_number_of_correct_haps(haps, np.array(i)) for i in unique_phasings]

            # add the current phasing to a list of all phasings
            if 4 not in scores:
                unique_phasings += [haps]
                unique_phasings_count += [1]
            else:
                scores_ind = scores.index(4)
                unique_phasings_count[scores_ind] += 1

            # check if you find unique haplotypes and count how many times you see a haplotype out of 100 times. 
            for i in haps:
                if tuple(i) in all_haps:
                    all_haps[tuple(i)] += 1/iter_n
                else:
                    all_haps[tuple(i)] = 1/iter_n

        # determine the scores for the phasings.  
        phasings_scores = []
        for index, i in enumerate(unique_phasings):
            score = 1
            for j in list(set([tuple(k) for k in i])):
                score *= all_haps[tuple(j)]
            phasings_scores += [[index, unique_phasings_count[index]/iter_n, score/4, unique_phasings_count[index]/iter_n * score/4]]


        phasings_scores.sort(key=lambda x: x[3], reverse=True)

        #add ONLY haplotypes from best phasing to reference library, to determine frequency

        for i in unique_phasings[phasings_scores[0][0]]:
            if tuple(i) in all_haplotypes_frequency:
                all_haplotypes_frequency[tuple(i)] +=1
            else:
                all_haplotypes_frequency[tuple(i)] = 1

        # determine ratios between best and 2nd best score
        if len(phasings_scores) != 1:
            ratio =  phasings_scores[0][3] / phasings_scores[1][3]
        else:
            ratio = 1e5
        
        print 'Ratio between best', ratio
        ratios += [ratio]

        # get the best haplotype
        haps = unique_phasings[phasings_scores[0][0]]
        for i in haps:
            final_haps += [i]

    # Optionally we could perform an additional round of checking using ALL of the computed haplotype solutions. 
    # not implemented yet!

    # write this in new file. 

    uni = []
    for i in all_haplotypes_frequency:
        # print i, all_haplotypes_frequency[i]
        u = [str(j) for j in i] + [str(all_haplotypes_frequency[i])]
        uni += [u]
    
    np.savetxt(output+'.unique', np.array(uni, dtype='str'), fmt='%s')

    final_haps = np.array(final_haps, dtype='str')
    # print final_haps.shape
    np.savetxt(output, final_haps, fmt='%s')
    np.savetxt(output+'.ratio', np.array(ratios))
def fitness(H, phasings):
    '''probabilityfitness'''
    k = H.shape[0]

    mismatches = 0
    total = len(phasings)*k
    # print phasings
    
    if total == 0:
        return 0.0

    match_score = 0
    for pair in phasings:
        part_H = H[:,pair]
        pairwise = phasings[pair]
        
        match_score  += get_number_of_correct_haps(pairwise, part_H)

    match_score =  match_score / total
    
    return match_score

def get_number_of_correct_haps(haps, selection):
    score = 0
    sel = list(selection.tolist()[:])
    for hap in haps:
        if hap in sel:
            score += 1
            sel.pop(sel.index(hap))
    return score

def full_fit(phasings_nodes, phasings_all):
    fit = []
    comb = []

    for i in phasings_all:
        fit += [fitness(np.array(i), phasings_nodes)]
        comb += [np.array(i)]

    

    return comb[fit.index(max(fit))].tolist(), fit

def simple_sampling(phasings_nodes, all_haps):
    
    # initialize a haplotype cosistent with dosage
    
    s = np.random.randint(len(all_haps), size=4)
    estimated_hap = np.array([all_haps[i] for i in s])
    iteration = 0
    score = 0
    
    while iteration < 500:
                
        s = np.random.randint(4)
        temp_hap = np.copy(estimated_hap)
        temp_hap[s] = all_haps[np.random.randint(len(all_haps))]
        temp = fitness(temp_hap, phasings_nodes)

        if temp > score:
            score = temp
            estimated_hap = temp_hap
        iteration += 1
    return estimated_hap,  fitness(estimated_hap, phasings_nodes)

def approximate_fit(phasings_nodes, all_haps, n=10):
    
    fits = []
    comb = []
    
    for i in range(n):
        H, fitness_hap = simple_sampling(phasings_nodes, all_haps)
        if sorted(H.tolist()) not in comb:
            comb += [sorted(H.tolist())]
            fits += [fitness_hap]
    
    # get the links

    best_indexes = [index for index, i in enumerate(fits) if i == max(fits)]
    ind = np.random.choice(best_indexes)
    return comb[ind], fits

## haplotyping
def split_hap(g):
    ''' 
    Split 00|01|00|11 in list. 
    '''
    snp1 = g.split('|')
    return [[j for j in list(i)] for i in snp1]

def create_phasings_nodes(indices, genotype):
    phasings_nodes = {}

    for index, i in enumerate(indices):
        gen = genotype[index]
        hap = split_hap(gen)
        phasings_nodes[i] = hap
    
    return phasings_nodes

class hybrid_joining:
    def __init__(self, phasings_nodes, blocks, dosages,  missing, reflibrary, k, X=100000):
        self.phasings_nodes = phasings_nodes
        self.blocks = blocks
        self.dosages = dosages
        self.missing = missing # do we need missing? We can also use 'N'
        self.ploidy = k
        self.X = X
        self.reflibrary = reflibrary #input reference library of haplotypes
        # print self.reflibrary
    def construct_haplotypes(self, block):
        ''' 
        The haplotypes are normally very easy. For each haplotpye you can have a set of permutations (24)
        '''

        # Each block should be a numpy array [[0000], [0000]]
        # the function will use the previously computed haplotypes and extend it. 
        options = list(itertools.permutations(block, self.ploidy))
        # print options[0]
        phasings = []

        for i in options:
            ph = []
            for index, j in enumerate(i):
                ph += [self.haplotypes[index] + j]
            # return only unique phasings
            
            duplicate = False

            for i in phasings:
                if get_number_of_correct_haps(i, np.array(ph)) == 4:
                    duplicate = True

            if duplicate == False:
                phasings += [ph]
        return phasings

    def compitability(self, phasing):
        '''compatibility checking with reflibrary'''
        score = 0
        # check if 'N'  in hap

        if 'N' in phasing[0]:
            Nindex = [index for index, i in enumerate(phasing[0]) if i == 'N']
        else:
            Nindex = []

        indices = [i for i in range(len(phasing[0])) if i not in Nindex]

        for i in phasing:
           
            if [i[x] for x in indices] in self.reflibrary[:,indices].tolist():
                score +=1
        return score/self.ploidy
    
    def best_match(self, phasings, combis):
        '''
        Return best match to phasings using the dissimilarity phasing
        And check compatibility with input haplotypes. if not any reference haplotypes, don't bother. 
        '''
        # return best match of pairwise phasings 

        fit = []
        comb = []

        ref_lib_comp = []

        for i in phasings:

            if self.reflibrary != None:
                score = self.compitability(i)
            else:
                score = 1.0
            
            fit += [fitness(np.array(i), combis) * score]
            comb += [np.array(i)]
            ref_lib_comp += [score]

        # Select one of the phasings which is the best. 
        best_indexes = [index for index, i in enumerate(fit) if i == max(fit)]
        ind = np.random.choice(best_indexes)

        return comb[ind].tolist(), fit

    def join(self):

        # initiate start of haplotype
        self.haplotypes = self.blocks.pop(0) # start block which will be the first snp or multiple snps
        self.length = len(self.haplotypes[0]) # the length of the already reconstructed haplotype
        # continue with the rest of the snps

        # print 'start joining', self.length


        while self.blocks != []:

            block = self.blocks.pop(0)
            # print 'block', block
            phasings = self.construct_haplotypes(block)
            self.length += len(block[0])
            # print 'length', self.length
            # Get the pairwise phasings corresponding to these indices. 
            needed_combis = list(itertools.combinations(range(0, self.length), 2))
            filter_combis = [i for i in needed_combis if i in self.phasings_nodes]
            
            selected_phasings = {index:self.phasings_nodes[index] for index in filter_combis}

            #print selected_phasings
            # get match between potential phaisngs and pairwise phasings. 
            
            H, C = self.best_match(phasings, selected_phasings)
            #print C, H
            # assert phasings == []
            # replace the current haplotype by a extended haplotype
            self.haplotypes = H
        for i in self.haplotypes:
            print ' '.join(i)        


    def join_random(self, random_option=False):


        indices = range(0, len(self.blocks))
        indices_used = []

        if random_option != False:
            random.shuffle(indices)

        # print 'indices', indices
        # initiate start of haplotype

        u = indices.pop(0)
        indices_used += [u]

        self.haplotypes = self.blocks[u] # start block which will be the first snp or multiple snps
        self.length = len(self.haplotypes[0]) # the length of the already reconstructed haplotype
        # continue with the rest of the snps

        while indices != []:

            u = indices.pop(0)
            block = self.blocks[u]
            # print 'block', block
            phasings = self.construct_haplotypes(block)
            self.length += len(block[0])

            # Get the pairwise phasings corresponding to these indices. 
            needed_combis = [tuple(sorted(i)) for i in itertools.product(indices_used, [u])] # get used combis and sort them
            filter_combis = [i for i in needed_combis if i in self.phasings_nodes]
            
            selected_phasings = {index:self.phasings_nodes[index] for index in filter_combis}

            # get match between potential phaisngs and pairwise phasings. 
            
            H, C = self.best_match_random(phasings, selected_phasings, indices_used, u)
            #print C, H
            # assert phasings == []
            # replace the current haplotype by a extended haplotype
            self.haplotypes = H
            indices_used += [u]
        
        # change order
        # for i in self.haplotypes:
            # print ' '.join(i)
        # print indices_used

        converted_indices = []
        for i in range(len(indices_used)):
            converted_indices += [indices_used.index(i)]
        
        # print converted_indices
        # print
        # for i in np.array(self.haplotypes, dtype='str')[:,converted_indices].tolist():
            # print ' '.join(i)
        # reorder haplototypes

        self.haplotypes = np.array(self.haplotypes, dtype='str')[:,converted_indices]

    def best_match_random(self, phasings, combis, indices_used, u):
        '''
        Return best match to phasings using the dissimilarity phasing
        Used for random input order. 
        '''
        # return best match of pairwise phasings 

        fit = []
        comb = []

        for i in phasings:

            fit += [self.fitness_random(np.array(i), combis, indices_used, u)]
            comb += [np.array(i)]

        # Select one of the phasings which is the best. 
        best_indexes = [index for index, i in enumerate(fit) if i == max(fit)]
        ind = np.random.choice(best_indexes)

        return comb[ind].tolist(), fit

    def fitness_random(self, H, phasings, indices_used, u):
        '''probabilityfitness'''
        k = H.shape[0]

        mismatches = 0
        total = len(phasings)*k
        # print phasings
        
        if total == 0:
            return 0.0

        match_score = 0
        
        # for each pairwise phasing that is provided calcualte the match_score
        
        for pair in phasings:

            if pair[0] == u:
                a = indices_used.index(pair[1])
                part_H = H[:,[-1,a]]
            else:
                a = indices_used.index(pair[0])
                part_H = H[:,[a,-1]] 
            
            pairwise = phasings[pair] # is the same 
            
            match_score  += get_number_of_correct_haps(pairwise, part_H)

        match_score =  match_score / total
        
        return match_score

def get_all_haps(hl, k=4, dosages=False, return_combi=False):
    
    if dosages == False:
        haps = list(itertools.product([0,1], repeat=hl))
        if return_combi == True:
            combis = list(itertools.combinations_with_replacement(haps, k))
        else:
            combis = []
        return haps, combis
    else:
        
        combis = [[]*4]
        
        for i in dosages:
            new_combis = []
            
            vect = [0,0,0,0]
            for k in range(i):
                vect[k] = 1
            
            adding = list(set(list(itertools.permutations(vect))))
            for option in adding:
                for j in combis:
                    new_combis += [j + list(option)]            
        
            combis = new_combis
        
        new_combis = []
        for j in combis:
            new_combis += [np.reshape(j, (4,hl))]
        
        return [], new_combis


if __name__ == '__main__':
    description_text = 'Haplotype inference using the EM algorithm.'
    epilog_text = 'Copyright Johan Willemsen May 2016'

    parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
    parser.add_argument('-i', type=str,help='Input blocks file, if - then read from stdin')
    parser.add_argument('-alleles', type=str,help='Input blocks file, if - then read from stdin')
    parser.add_argument('-haplotypes', type=str,default = '-', help='Input haplotypes file, if - then read from stdin')
    parser.add_argument('-reflibrary', type=str,default = '-', help='Input reference haplotype library')

    parser.add_argument('-o', type=str, default='-', help='Output file name, if nothing is specified then put to stdout' )
    parser.add_argument('-interval_length', type=int, default=4, help='Length of the interval used for haplotyping, default is 4')
    parser.add_argument('-ploidy', type=int, default=4, help='The ploidy level')

    parser.add_argument('-X', type=int, default=100000, help='overlapping blocks')

    args = parser.parse_args()

    allele_file = np.loadtxt(args.alleles, dtype='str').T
    no_cultivars = allele_file.shape[0]

    final_haps = []

    alleles =  np.loadtxt(args.alleles, dtype='str')
    block_file = args.i 

    # old version
    #wrap_join(alleles, block_file, args.ploidy, args.X, args.o)
    
    # new verstion

    if args.haplotypes == '-':
        haplotypes = []

    if args.reflibrary != '-':
        reflibrary = np.loadtxt(args.reflibrary, dtype='str')
    else:
        reflibrary = None

    # print reflibrary

    wrap_join_hybrid(alleles, haplotypes, block_file, args.ploidy, args.X, args.o, reflibrary)

    #python divide_and_conquer.py -i .\example_data\hap.blk -alleles .\example_data\gwd.all.dat
    #python wrapper.py -i ./example_data/gwd_alleles.dat -o ./example_data/hap -ploidy 4 -interval_length 4 -skip 1 -em 

    #python divide_and_conquer.py -i .\example_data\hap.blk -alleles .\example_data\gwd.all.dat -o test.txt -reflibrary ref_hap.txt
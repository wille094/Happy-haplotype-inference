
import numpy as np 
import itertools
import argparse

def get_number_of_correct_haps(haps, selection):
    '''
    Quantitative score, how many haplotypes are correct, with dosage estimation.
    '''
    score = 0
    sel = list(selection[:])
    for hap in haps:
        if hap in sel:
            score += 1
            sel.pop(sel.index(hap))
        else:
            pass
    return score

class haplojoin:
    def __init__(self, blocks, ploidy, step_number, iter_no, length_haplotype):
        self.blocks = blocks
        self.ploidy = ploidy
        self.step_number = step_number
        self.iter_no = iter_no
        self.length_haplotype = length_haplotype
        self.error = False
        self.error_indices = []

    def generate_counts(self, haps):
        '''
        Generate score matrix from a array with haplotype observations of two
        '''
        allele = [0,1]
        count_mat = np.zeros((2,2))
        for H in haps:
            count_mat[int(H[0]),int(H[1])] +=1
        return count_mat
    
    def get_haplotype_frame(self, blocks):

        block_keys = blocks.keys()
        seed_block = None
        seed_hap = {}
        used_indices = []
        for i in blocks:
            temp_block = blocks[i]
            x, y = np.where(temp_block == temp_block.sum())
            
            if len(x) == len(y) == 1:
                seed_hap[i[0]] = int(x)
                seed_hap[i[1]] = int(y)
                used_indices += [i]


        return seed_hap, used_indices


    def get_seed_block(self, blocks, rest_snp_indices, used_indices = [], verbose=False):
        
        block_keys = list(itertools.combinations(rest_snp_indices, 2))
        # print 'rest_block_keys', block_keys
        block_keys = [i for i in blocks.keys() if i not in used_indices]

        seed_block = None
        all_keys = np.arange(len(block_keys))
        np.random.shuffle(all_keys)
        all_keys = all_keys.tolist()
        
        while seed_block == None or all_keys != [] :
            s = all_keys.pop(0)
            temp_block = blocks[block_keys[s]]
            
            a = np.where(temp_block != 0)[0].tolist()
            b = np.where(temp_block != 0)[1].tolist()
            
            if len(a) > 0:
                seed_block = block_keys[s]
        
        choice = np.random.randint(0, len(a))
        blocka_base = a[choice]
        blockb_base = b[choice]
        
        H = [blocka_base, blockb_base]
        
        if seed_block == None:
            #self.error = True
            return False, False

        else:
            return H, seed_block

    def check_uniqueness_off_block(self, score_mat, index_S1, index_S2):
        '''
        A block is unique if a phasing occurs once, or when no other phasings have that allele. 
        In both cases it is the number of indices that is > 2. 
        '''
        #print score_mat
        
        if index_S1 == None:
            # then select columns
            selected_phasing = score_mat[:,index_S2]
        else:
            selected_phasing =  score_mat[index_S1,:] 
        
        connections = np.where(selected_phasing > 0)[0]
        # print selected_phasing.sum(),
        if len(connections) == 1:
            #print 'U, so SNP', index_S1, 'connected to', connections, [index_S1, connections[0]]
            return True, connections[0]
        else:
            return False, None
                
    def get_outward_connections(self, hap_dict, trace_keys, block_keys):
        '''
        Getting outward connections of file. 
        '''
        k = hap_dict.keys()
        outwards_connections = [i for i in block_keys if i[0] in k or i[1] in k]

        for i in trace_keys:
            if i in outwards_connections:
                outwards_connections.remove(i)
        return outwards_connections


    def experimental_iter_extension(self, hap, rest_snp_indices, blocks):

        # Select random SNP that is present not in the input haplotype

        s = np.random.randint(0,len(rest_snp_indices))
        snp_index = rest_snp_indices.pop(s)

        # For all connections to SNPs that are present in the input haplotype determine which connection is unique. 
        # Should we choose here the one with the highest likelihood or gls?
        
        score_hap = []
        score_hapf = []

        for used_snp in hap:
            indices = sorted([used_snp, snp_index])
            # print 'indices', indices
            if tuple(indices) in blocks:
                temp_block = blocks[tuple(indices)]
                S2b  = self.determine_uniqueness_iter(temp_block, hap, indices)
                
                #print snp_index, indices, S2b
                
                if S2b is not False: # so unique extension
                    score_hap += [S2b]
                else:
                    score_hapf += [S2b]
            else:
                pass
                # print 'removed becuase of low confidece'

        # Hence given a X ambigious SNP in the input hap to the target SNP (snp_index) a False or bases is presented. 
        # In the first iteration we have 2 ambigious snps (seed block), hence two connections, and if disagreeement, random selection of a base. 
        # Subsequent iterations will have more. 

        # If the list of unique alleles is bigger than 0, then choose the one which has the highest likelihood? 

        if score_hap != []:
            unique_alleles = list(set(score_hap))
            number_alleles = []

            for i in unique_alleles:
                number_alleles += [score_hap.count(i)]

            max_score = number_alleles.index(max(number_alleles))

            hap[snp_index] = unique_alleles[max_score]
        # else:
            # rest_snp_indices += [snp_index]

        return hap, rest_snp_indices

    def determine_uniqueness_iter(self, temp_block, hap, indices):
        '''The uniqueness of a block given a score mat'''
        S1, S2 = indices

        if S1 in hap:
            S1u, S1b = self.check_uniqueness_off_block(temp_block, hap[S1], None)

            if S1u == True:
                return S1b
            
        elif S2 in hap:
            S1u, S1b = self.check_uniqueness_off_block(temp_block, None, hap[S2])
            
            if S1u == True:
                return S1b

        return S1u


    def iter_get_extension(self, hap, trace_keys, block_keys, outwards_connections, blocks, verbose=False):
        '''
        Get links, and 
        ''' 

        s = np.random.randint(0,len(outwards_connections))  # Get a random link
        temp_block = blocks[outwards_connections[s]] # so say 0 0 and 0 1, then also 01

        S1, S2 = outwards_connections[s]

        # Get which allele to count as the connection
        if S1 in hap:
            S1u, S1b = self.check_uniqueness_off_block(temp_block, hap[S1], None)

            if S1u == True:
                hap[S2] = S1b
            
        elif S2 in hap:
            S1u, S1b = self.check_uniqueness_off_block(temp_block, None, hap[S2])
            
            if S1u == True:
                hap[S1] = S1b

        # all inidices in outwards_connections that have target snp and any of the occuring snps in the haplotype

        trace_keys = trace_keys + [outwards_connections[s]]
        return trace_keys, hap

    def joining2(self, blocks, length_hap, verbose=False, step_number=500):
        '''Joining experimental'''
        
        hap = {}
        block_keys = blocks.keys()
        
        # Get seed haplotype (so step 0) consisting out of the frame haplotype (homozygous phasings)
        # This seed hap is not instrumental to the haplotype phasing, it's just filling in the homozygous snps, which will be used at the end.  

        seed_hap, used_indices = self.get_haplotype_frame(blocks)
        # print 'haplotype frame', seed_hap

        # Get all indices of SNPs present in the blocks, except the homozygous SNP combinations.
        rest_snp_indices = []
        for i in block_keys:
            rest_snp_indices = rest_snp_indices + list(i)

        rest_snp_indices = list(set(rest_snp_indices))
        # print rest_snp_indices# , used_indices

        # Remove homozygous snps
        for i in seed_hap:
            if i in rest_snp_indices:
                k = rest_snp_indices.remove(i)

        # Create hap
        for i in seed_hap:
            hap[i] = seed_hap[i]

        if len(seed_hap.keys()) == length_hap:
            # print 'ddd'
            return hap

        # Get a seed within the blocks not used,  

        H, seed_block = self.get_seed_block(blocks, rest_snp_indices, used_indices=used_indices)
        # print 'seed block', seed_block, H


        # assert 2==4
        #removed used indices from seed block
        for index, i in enumerate(seed_block):
            hap[i] = H[index]
            if i in rest_snp_indices:
                k = rest_snp_indices.remove(i)

        # print 'after seedblock construction', rest_snp_indices

        steps = 1

        # Use the rest of te unambigious keys to recconstruct the whole haploype. 

        while steps < step_number and len(hap.keys()) < length_hap:
            if len(rest_snp_indices) != 0:
                hap, rest_snp_indices = self.experimental_iter_extension(hap, rest_snp_indices, blocks)
                # print hap, rest_snp_indices
            else:
                pass
            steps +=1
        return hap


    def joining(self, blocks, length_hap, verbose=False, step_number=1):
        
        hap = {}
        block_keys = blocks.keys()
        
        ############################################################
        # Get seed haplotype (so step 0) (Can be put in one function)
        
        H, seed_block = self.get_seed_block(blocks)
        
        for index, i in enumerate(seed_block):
            hap[i] = H[index]
        
        #print seed_block, hap, H

        trace_keys = [seed_block]
                
        steps = 1

        while steps < step_number and len(hap.keys()) < length_hap:
            outwards_connections = self.get_outward_connections(hap, trace_keys, block_keys)
            if len(outwards_connections) != 0:
                #print steps,
                trace_keys, hap = self.iter_get_extension(hap, trace_keys, block_keys, outwards_connections, blocks, verbose=verbose)
                #print hap, trace_keys[-1]
            else:
                pass
            steps +=1
        #print  hap.values(), len(outwards_connections)
        return hap

    def get_individual_haplotype(self, block_dict, length_hap, iter_no=500, step_number=500):
        steps_array = []
        for i in range(1):
            step = 1
            # hap = self.joining(block_dict, length_hap, step_number = step_number)
            hap = self.joining2(block_dict, length_hap, step_number = step_number)
            while len(hap) < length_hap and step < iter_no:
                #print(len(hap))
                # print 'new'
                # hap = self.joining(block_dict, length_hap, step_number = step_number)
                hap = self.joining2(block_dict, length_hap, step_number = step_number)
                step+=1
            if len(hap) < length_hap:
                print 'Error not enough steps'
                # print block_dict
                #self.error = True
                #return None
            else:
                steps_array += [hap]
        
        for H in steps_array:
            H_index = sorted(H.keys())
            new_H = []
            for i in H_index:
                new_H += [H[i]]
            # print new_H
        
        if steps_array == []:
            return None
        else:
            return steps_array[0] #hap




    def get_haplotypes(self, length_hap):
        block_dicts = []
        print 'new'
           
        hap1 = self.get_individual_haplotype(self.block_dict, length_hap)
        # print 'new1', hap1
        if hap1 == None:
            return False
        block_dict1 = self.update_block_dict(self.block_dict, hap1)
        block_dicts += [block_dict1]

        hap2 = self.get_individual_haplotype(block_dict1, length_hap)
        # print 'new2', hap2

        if hap2 == None:
            return False
        block_dict2 = self.update_block_dict(block_dict1, hap2)
        block_dicts += [block_dict2]
        
        hap3 = self.get_individual_haplotype(block_dict2, length_hap)
        # print 'new3', hap3

        if hap3 == None:
            return False
        block_dict3 = self.update_block_dict(block_dict2, hap3)
        block_dicts += [block_dict3]
        
        hap4 = self.get_individual_haplotype(block_dict3, length_hap)
        # print 'new4', hap4

        if hap4 == None:
            return False
        block_dict4 = self.update_block_dict(block_dict3, hap4)
        block_dicts += [block_dict4]
        

        haplotypes = [hap1, hap2, hap3, hap4]
        self.est_haplotypes = haplotypes
        self.block_dicts = block_dicts
        return True

    def update_block_dict(self, blocks, haplotype):
        new_block_dict = {}
        for i in blocks:
            # print i, 
            alleleS1 = haplotype[i[0]]
            alleleS2 = haplotype[i[1]]
           
            #print blocks[i]
            new_block = np.copy(blocks[i])
            new_dosage = new_block[alleleS1][alleleS2] - 1
            #print i, alleleS1, alleleS2, new_dosage
            if new_dosage < 0:
                self.error = True
                print 'E', i
                self.error_indices += [i]
            new_block[alleleS1][alleleS2] = new_block[alleleS1][alleleS2] - 1
            # print new_block
            # assert new_block.sum() == 3
            new_block_dict[i] = new_block
        return new_block_dict

    def construct_block_dict(self, haplotypes):
        block_dict = {}
        for i in self.blocks:            
            score_mat =  self.generate_counts(haplotypes.T[list(i)].T)
            block_dict[i] = score_mat
        
        self.block_dict = block_dict

    def construct_block_dict_on_phases(self, haplotypes):
        '''
        array([[ 0.,  1.],
       [ 1.,  0.],
       [ 0.,  0.],
       [ 0.,  1.]])
        Untested''' 
        
        block_dict = {}
        for i in haplotypes:
            try:
                score_mat = self.generate_counts(haplotypes[i])
                block_dict[i] = score_mat
            except:
                pass
        self.block_dict = block_dict
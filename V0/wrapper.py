#!/home/johan/anaconda2/bin/python

from __future__ import division
import numpy as np
import scipy 
import os
import itertools
from scipy.special import gamma, gammaln
import math
from numpy import array, log, exp
import operator
import argparse
import time
import datetime

# Import the haplotype inference script
from inference_em import *

# Import the haplojoin v1 script. 
from haplojoin import haplojoin

# Import the haplojoin v2 script

from divide_and_conquer import *

# import the funcitons to read in haplotype blocks
from read_blocks import *

def convert_allele_dosage(ploidy, g):
    # convert allele dosage (1) to [1, 0, 0, 0,] or [NA x 4] in case of missing data
    snp1 = ['0'] * ploidy
    try:
        for j in range(int(g)):
            snp1[j] = '1'
        return sorted(snp1)
    except:
        return ['NA'] * ploidy

def convert_allele_matrix(ploidy, genotype_matrix):
    # Convert all snps to blocks.
    conv_array = np.array([[convert_allele_dosage(ploidy, i) for i in row.tolist()] for row in genotype_matrix])
    return conv_array

class haplotype_wrapper(haplotype_inference):
    def __init__(self, genotype_matrix, ploidy, interval_length, skip, steps, output_file, EM_ratio=0.95, sampling_no=50):
        self.ploidy = ploidy
        self.genotype_matrix = genotype_matrix
        self.interval_length = interval_length
        self.no_markers = genotype_matrix.shape[0]
        self.no_genotypes = genotype_matrix.shape[1]
        self.skip = skip
        self.steps = steps
        self.EM_ratio = EM_ratio
        self.sampling_no = sampling_no
        self.output_file = output_file

    def convert_array(self, gen_mat):
        '''
        convert genotype array
        (markers x genotypes)  to [alleles] * markers * genotypes
        '''
        # Here the markers [1|0|0|0] are split in separate alleles
        #conv_array = [map(self.split_hap,i) for i in gen_mat.tolist()]

        conv_array = gen_mat # hack

        no_snps = len(conv_array)
        no_obs = len(conv_array[0])

        # For each observation the converted array is read.
        b = []
        for obs in range(no_obs):
            row = tuple([sorted(conv_array[snp][obs] ) for snp in range(no_snps)])
            b += [row]
        return b 

    def convert_prob_array_to_obs_array(self, genotype_matrix):
        '''
        The input is a table with x rows (no markers) and y columns (no possible genotypes * obs)
        The output should be a array with a cross product of those probabilities. these probability is the observation. 
        
        '''

        observation_matrices = []

        for i in range(0, genotype_matrix.shape[1], 5):
            slice_array = genotype_matrix[:,i:i+5]
            obs_gen = slice_array.tolist()
            genotype_probabilities = np.product(np.array(list(itertools.product(*obs_gen))), axis=1)
            ### Here: NN NS ND NT NQ
            observation_matrices += [genotype_probabilities]

        return observation_matrices

    def join(self, final_save, no_cultivars, indices):
        haplotype_matrix = []
        str_hap_matrix =  [] 
        errors = 0
        errors_cv = []
        errors_indices = []

        for cv_index in range(no_cultivars):
            print 'cultivar', cv_index
            
            blocks = indices

            final_cultivar = {}
            final_cv_blocks = []

            for i in blocks:
                if 'NA' in final_save[i][cv_index]:
                    # print final_save[i][cv_index]
                    pass
                else:
                    final_cultivar[i] = final_save[i][cv_index]
                    final_cv_blocks += [i]
            
            assert len(final_cultivar) == len(final_cv_blocks)

            hapjoin = haplojoin(blocks, 4, 500, 500, self.interval_length)
            hapjoin.construct_block_dict_on_phases(final_cultivar)
            success = hapjoin.get_haplotypes(self.interval_length)

            if success == True:

                haplotypes_est_arr = []

                for H in hapjoin.est_haplotypes:
                    H_index = sorted(H.keys())
                    new_H = []
                    for i in H_index:
                        new_H += [H[i]]
                    haplotypes_est_arr += [new_H]

                # haplotypes_est_arr = np.array([i.values() for i in hapjoin.est_haplotypes])
                haplotype_matrix += [np.array(haplotypes_est_arr)]

            else:
                haplotypes_est_arr = np.zeros((self.ploidy, self.interval_length), dtype='str')
                haplotypes_est_arr.fill('N')

            if hapjoin.error == True:
                # print '#################### EERRRORRRRRR #######'
                errors += 1
                errors_cv += [cv_index]
                errors_indices += [hapjoin.error_indices]
            # print haplotypes_est_arr
            str_hap_matrix += ['|'.join([''.join([str(b) for b in hap]) for hap in haplotypes_est_arr])]
        
        # for index, i in enumerate(str_hap_matrix):
            # print index, i

        # print 'total number of errors are', errors
        # print errors_cv
        # for i in errors_indices:
            # print i
        return str_hap_matrix
    
    def generate_combis(self, X):

        for i in itertools.combinations(range(self.no_markers),2):
            if i[1] - i[0] <= X:
                yield i

    def only_EM_hap_sliding(self, GBS=False, X=False):

        gl_file = open(self.output_file + '.gls', 'wb')
        lr_file = open(self.output_file + '.lr', 'wb')
        bl_file = open(self.output_file + '.blk', 'wb')
        fr_file = open(self.output_file + '.frq', 'wb')
        
        self.new_genotype_matrix = []
        if X == False:
            thres = self.no_markers + 1
        else:
            thres = X

        for a in xrange(0, self.no_markers-self.interval_length, self.skip):

            # print range(a, a + self.interval_length) # the combination used of snps indices
            i = tuple(range(a, a + self.interval_length))
            if GBS == True:
                subset_mat = np.array(self.genotype_matrix[list(i),:], dtype="float")
                observation_matrices = self.convert_prob_array_to_obs_array(subset_mat)
                haplotyping = haplotype_inference(observation_matrices, self.ploidy,self.interval_length)
                haps = list(itertools.product(['0','1'], repeat=self.interval_length))
                # print haps
                haplotyping.get_genotypes_prob(haps=haps, prune=False)
                haplotyping.genotype_decomposition_probability()

                # print haplotyping.decomposed_genotype_array.shape
                # print '###########'

            elif GBS == False:
   
                subset_mat = np.array(self.genotype_matrix[list(i), :])

                # TODO: Return list of observations instead of conversion.
                converted_test = self.convert_array(subset_mat) # split obs in alleles and return list of obs
                haplotyping = haplotype_inference(converted_test, self.ploidy, self.interval_length)

                haplotyping.get_alleles()

                haplotyping.get_genotypes()
                haplotyping.genotype_decomposition()

            for j in range(self.steps):
                # step j
                haplotyping.initialize_haplotype_priors()
                haplotyping.expected_genotype_count()
                haplotyping.iter_em()

            haplotyping.select_best_run()                   #select best run
            haplotyping.create_haplotypes()

            freq = [''.join(b) for b in haplotyping.haps] + [str(b) for b in haplotyping.real_freq]

            # save the pairwise marker
            gl_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + '.' + '\t' + '\t'.join([str(j) for j in haplotyping.GLs.tolist()]) + '\n')
            lr_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + '.' + '\t' + '\t'.join([str(j) for j in haplotyping.LRs]) + '\n')
            bl_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + '.' + '\t' + '\t'.join(['|'.join([''.join(k) for k in j]) for j in haplotyping.phased_real]) + '\n')
            fr_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + '.' + '\t'.join(freq) + '\n')

        gl_file.close()
        lr_file.close()
        bl_file.close()
        fr_file.close()


    def mode_array(self, x, common=False):
        
        scores = []
        modes = []
        
        for i in np.array(x).T:
            i_conv = [int(j) for j in i.tolist() if j != 'NA']    
            # print i_conv

            if i_conv == []:
                scores += ['NA']
                modes += ['NA']
                continue
            
            score = np.bincount(i_conv)
        
            # get relative proportion of best scores
            if len(score) != 1:
                sm, m = np.argsort(score)[-2:]
                if common == False:
                    scores += [score[m] / (score[m] + score[sm])]
                else:
                    scores += [score[m] / np.array(score).sum()]
                modes += [str(m)]
            else:
                m = np.argsort(score)[0]
                scores += [score[m] / score[m]]
                modes += [str(m)]
        return scores, modes
        

    def construct_pairwise_phasings(self, GBS=False, X=False):
        '''
        Construct pairwise phasings using either GBS or not. 
        Dependend on:
        1. haplotyping inference
        2. self.generate_combis

        '''
        gl_file = open(self.output_file + '.gls', 'wb')
        lr_file = open(self.output_file + '.lr', 'wb')
        bl_file = open(self.output_file + '.blk', 'wb')
        fr_file = open(self.output_file + '.frq', 'wb')

        rep_file = open(self.output_file + '.rep', 'wb') # contains the replicate values
        

        self.new_genotype_matrix = []

        if X == False:
            thres = self.no_markers + 1
        else:
            thres = X
        print datetime.datetime.now().strftime("%d-%m-%y\t%H:%M:%S\t"),
        print 'Construct pairwise phasings...',

        for i in self.generate_combis(thres):
            #print i # the combination used of snps indices

            if GBS == True:
                subset_mat = np.array(self.genotype_matrix[list(i),:], dtype="float")
                observation_matrices = self.convert_prob_array_to_obs_array(subset_mat)
                haplotyping = haplotype_inference(observation_matrices, self.ploidy,self.interval_length)
                
                haps = list(itertools.product(['0','1'], repeat=self.interval_length))
                # print haps
                haplotyping.get_genotypes_prob(haps=haps, prune=False)
                haplotyping.genotype_decomposition_probability()

                # print haplotyping.decomposed_genotype_array.shape
                # print '###########'

            elif GBS == False:
   
                subset_mat = np.array(self.genotype_matrix[list(i), :])

                # TODO: Return list of observations instead of conversion.
                converted_test = self.convert_array(subset_mat) # split obs in alleles and return list of obs
                # print len(converted_test)

                haplotyping = haplotype_inference(converted_test, self.ploidy, self.interval_length)
                haplotyping.get_alleles()
                haplotyping.get_genotypes()
                haplotyping.genotype_decomposition()

            for j in range(self.steps):
                # step j
                haplotyping.initialize_haplotype_priors()
                haplotyping.expected_genotype_count()
                haplotyping.iter_em()

            haplotyping.select_best_run()                   #select best run
            haplotyping.create_haplotypes()

            H_array = [haplotyping.phased_indices]

            rep_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + 'full' + '\t' + '\t'.join(haplotyping.phased_indices) + '\n')

            # perform subsampling in genotype panel to test robustness of phasing. 
            
            for sample_number in range(self.sampling_no):
                H = self.sampling(converted_test)
                H_array += [H]
                # rep_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + str(sample_number) + '\t' + '\t'.join(H) + '\n')
            
            scores, modes = self.mode_array(H_array, common=False)

            # Agreement of full phasing with modus
            agreement = []
            for index, k in enumerate(H_array[0]):
                if k == modes[index]:
                    agreement += ['1']
                else:
                    agreement += ['0']
            
            # Only report phasings that are robust against subsampling.
            corrected_modes = []
            for index, m in enumerate(modes):
                if scores[index] > self.EM_ratio:
                    corrected_modes += [m]
                else:
                    corrected_modes += ['NA']

            
            rep_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + 'modes ' + '\t' + '\t'.join(modes) + '\n')
            rep_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + 'scores' + '\t' + '\t'.join(["%.2f" % k for k in scores]) + '\n')
            rep_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + 'agree ' + '\t' + '\t'.join([str(k) for k in agreement]) + '\n')
            rep_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + 'correct' + '\t' + '\t'.join([str(k) for k in corrected_modes]) + '\n')

            haplotyping.create_haplotypes(indices_modus=corrected_modes)


            freq = [''.join(b) for b in haplotyping.haps] + [str(b) for b in haplotyping.real_freq]

            # save the pairwise marker
            gl_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + '.' + '\t' + '\t'.join([str(j) for j in haplotyping.GLs.tolist()]) + '\n')
            lr_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + '.' + '\t' + '\t'.join([str(j) for j in haplotyping.LRs]) + '\n')
            bl_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + '.' + '\t' + '\t'.join(['|'.join([''.join(k) for k in j]) for j in haplotyping.phased_real]) + '\n')
            rep_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + '.' + '\t' + '\t'.join(['|'.join([''.join(k) for k in j]) for j in haplotyping.phased_real]) + '\n')
            fr_file.write(str(i[0]) + '\t' + str(i[1]) + '\t' + '.' + '\t'.join(freq) + '\n')

        gl_file.close()
        lr_file.close()
        bl_file.close()
        fr_file.close()
        rep_file.close()
        print 'Done!'

    def sampling(self, converted_test, prop=0.6):
        '''Function to bootstrap the pairwise phasings by choosing subsets of the population and calculate consistency
        '''

        selection_indices = np.random.choice(range(self.no_genotypes), int(prop*self.no_genotypes))
        subset_converted_test = [converted_test[i] for i in selection_indices]

        haplotyping = haplotype_inference(subset_converted_test, self.ploidy, self.interval_length)
        haplotyping.get_alleles()
        haplotyping.get_genotypes()
        haplotyping.genotype_decomposition()

        for j in range(1):
                # step j
            haplotyping.initialize_haplotype_priors()
            haplotyping.expected_genotype_count()
            haplotyping.iter_em()

        haplotyping.select_best_run()                   #select best run
        haplotyping.create_haplotypes()

        vect = 'NA'
        haplotypes = [vect] * len(converted_test)
        
        for index_j, j in enumerate(haplotyping.phased_indices):
        # for index_j, j in enumerate(haplotyping.phased_real):
            haplotypes[selection_indices[index_j]] = str(j)

        return haplotypes




 

if __name__ == '__main__':

    description_text = 'Haplotype inference using EM-based heuristic phasing'
    epilog_text = 'Copyright Johan Willemsen May 2016'

    parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
    
    # The -i option should allow to input the allele matrix (so dosages vs genotypes)
    parser.add_argument('-i', type=str,default='-', help='Input genotype matrix file, if - then read from stdin')
    parser.add_argument('-iblock', default='-', type=str,help='Input block file to skip pairwise phasing')

    parser.add_argument('-o', type=str, default='-', help='Output file name, if nothing is specified then put to stdout' )
    parser.add_argument('-interval_length', type=int, default=2, help='Length of the interval used for haplotyping, default is 4')
    parser.add_argument('-ploidy', type=int, default=4, help='The ploidy level')
    parser.add_argument('-skip', type=int, default=1, help='Number of markers to skip, normally over a sliding window of 1 snp')
    
    parser.add_argument('-steps', type=int, default=10, help='Number of iterations for EM')
    parser.add_argument('-sampling_no', type=int, default=1, help='Number of samples')
    parser.add_argument('-sampling_proportion', type=float, default=0.75, help='Number of samples')
    parser.add_argument('-joining_steps', type=int, default=10, help='Number of iterations for joining')

    # extra filtering arguments
    parser.add_argument('-EM_ratio', type=float, default=0.95, help='threshold for assignment')

    # Experimental options for GBS data and joining (should be shanged in exp)
    parser.add_argument('-gbs', action='store_true', help='experimental use of genotype probabilities to input')
    parser.add_argument('-em', action='store_true', help='experimental use of genotype probabilities to input')

    parser.add_argument('-join', action='store_true', help='join the haplotypes')

    # hybrid option for combining haplotype assembly and haplotype inference. 
    parser.add_argument('-hybrid', action='store_true', help='Hybrid between assembly and inferencc')

    # experimental options to produce only a block file for a region. 

    parser.add_argument('-X', type=int, default=100000, help='Number of snps to consider')
    parser.add_argument('-split', type=int, default=10000, help='split on X SNPs')
    parser.add_argument('-skip2', type=int, default=1, help='Number of markers to skip, normally over a sliding window of 1 snp')
    parser.add_argument('-split_list', type=str, default='-', help='Input_list of blocks (corresponding to markers')


    def sliding_window(n,length=10):
        # get blocks
        intervals = []
        
        for i in range(n-length):
            intervals += [range(i,i+length)]
        return intervals


    args = parser.parse_args()

    ploidy = int(args.ploidy)
    interval_length = int(args.interval_length)
    skip = int(args.skip)
    steps = int(args.steps)

    if args.i != '-' and args.hybrid != True:
        genotype_matrix = np.loadtxt(args.i, dtype='str') # markers > genotyeps
        


    
    if args.gbs != True and args.hybrid != True:
        # if no GBS data than do conversion before
        # convert genotype array to list of observations

        converted_genotype_array = convert_allele_matrix(ploidy, genotype_matrix)
    
    if args.hybrid == True: #TODO: Change to equal to
        # perform hybrid assembly

        # read in blocks
        bin_files = np.loadtxt(args.binfiles, dtype='str').tolist()
        # reconstruct allele genotype matrix. Based on this matrix we can construct pairwise phasings between snps. 
        
        all_blocks = []
        all_haps = []
        all_alleles = []

        for i in bin_files:
            blocks, haps, dosages = read_bins_file(i)
            all_blocks += [blocks]
            all_haps += [haps]
            all_alleles += [dosages]
        
        genotype_matrix = np.array(all_alleles).T
        # Reconstruct the individual blocks. 
        # perform pairwise phasing of alleles. 

        print 'parse'
        converted_genotype_array = convert_allele_matrix(ploidy, genotype_matrix)
        hap = haplotype_wrapper(converted_genotype_array, ploidy, interval_length, skip, thres, steps)

        print 'start pairwise'
        if args.gbs != True:
            hap.construct_pairwise_phasings(X=args.X, GBS=False)
        else:
            hap.construct_pairwise_phasings(X=args.X, GBS=True)

        # Use blocks as input

        
        wrap_join_hybrid(genotype_matrix, all_haps, args.o +'.blk', ploidy, args.X, args.o + '.long')

    elif args.em == True:
        # perform only the em
        hap = haplotype_wrapper(converted_genotype_array, ploidy, interval_length, skip, steps, EM_ratio=args.EM_ratio, sampling_no=args.sampling_no)
        
        if args.gbs != True:
            hap.only_EM_hap_sliding(GBS=False, X=args.X)
        else:
            hap.only_EM_hap_sliding(GBS=True, X=args.X)           


    else:
        
        if args.split_list != '-':
            
            blocks = np.loadtxt(args.split_list, dtype='S20')
            unique_blocks = np.unique(blocks)

            intervals = []

            for blk in unique_blocks:
                intervals += [np.where(blocks == blk)[0].tolist()]
            
            # print intervals
        else:
            # Get intervals if needed. 
            n = converted_genotype_array.shape[0]
            # Get phases over interval
            if args.split == 10000:
                intervals = [range(n)]
            else:
                length_interval = args.split
                intervals = sliding_window(n, args.split)

        
        assert intervals != []



        # perform phasing over all blocks over sliding window. 
        
        for index_int, interval in enumerate(intervals):

            converted_genotype_array_subset = converted_genotype_array[interval,]
            subset_genotype_array = genotype_matrix[interval,]
            # print genotype_matrix.shape
            # print subset_genotype_array.shape
            file_prefix = args.o + '.interval_' + str(index_int)
            # Perform pairwise phasing
            if args.iblock == '-':
                hap = haplotype_wrapper(converted_genotype_array_subset, ploidy, interval_length, skip, steps, file_prefix, EM_ratio=args.EM_ratio, sampling_no=args.sampling_no)
                
                if args.gbs != True:
                    hap.construct_pairwise_phasings(X=args.X, GBS=False)
                else:
                    hap.construct_pairwise_phasings(X=args.X, GBS=True)
                
                blk_file = args.o + '.interval_' + str(index_int) +'.blk' 
            else:
                blk_file = args.iblock

            # Joining? 
            if args.join == True:
                wrap_join_hybrid(subset_genotype_array, [], blk_file, ploidy, args.X, args.o  + '.interval_' + str(index_int) + '.long', None, iter_n=args.joining_steps) 

    print  datetime.datetime.now().strftime("%d-%m-%y\t%H:%M:%S\t"), 'Haplotype inference finished'

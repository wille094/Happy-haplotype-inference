import numpy as np
import itertools
import pandas as pd
import argparse

def get_number_of_correct_haps2(haps, selection):
    score = 0
    sel = list(selection.tolist()[:])
    wrong = []
    for index, hap in enumerate(haps.tolist()):
        if hap in sel:
            score += 1
            sel.pop(sel.index(hap))
        else:
            wrong += [index]
    return score, wrong

def get_number_of_correct_haps(haps, selection):
    score = 0
    sel = list(selection.tolist()[:])
    # print sel
    for hap in haps.tolist():
        if hap in sel:
            score += 1
            sel.pop(sel.index(hap))
    return score

def get_switch_error(haplotypes, infered_haplotype, ploidy=4):
    no_switches = 0
    where_switch = []
    
    new_infered = np.copy(infered_haplotype)
    
    for index, i in enumerate(range(2,haplotypes.shape[1])):
        temp_hap = haplotypes[:,:i]
        temp_inf = new_infered[:,:i]
        
        score, wrong = get_number_of_correct_haps2(temp_inf, temp_hap)
        if score < ploidy:
            
            no_switches += 1
            where_switch += [i]
            
        combinations = list(itertools.permutations(wrong, len(wrong)))
        
        wait_hap = np.copy(new_infered)
        
        while score != ploidy or combinations != [()]:
            
            u = combinations.pop() #(0,1, 1, 0)
            for index_k, k in enumerate(u):
                from_H = wrong[index_k]
                wait_hap[from_H,:i-1] = temp_inf[k,:-1]
            
            score, wrong2 = get_number_of_correct_haps2(wait_hap[:,:i], temp_hap)

            if score == ploidy:
                break
        
        new_infered = wait_hap
    return no_switches, where_switch

def pair_wise_accuracy(blocka, blockb, ploidy=4):
    '''
    Return pairwise accuracy estimate, of all possible pairs. 
    so 1-2, 1-3, But also dictionary of accuracy along intervals. 
    '''
    
    ploidy, length = blocka.shape
    options = list(itertools.combinations(range(length), 2))

    pairwise_dict = {}
    if options != []:
        pairwise_dict = {}
        pairwise_list = []
        score = []
        for i in options:
            
            haps = blocka[:,i]
            selection = blockb[:,i]

            score += [get_number_of_correct_haps(haps, selection)]
            pairwise_dict[i] = score
            pairwise_list += [score]
            
        #print len(pairwise_list)
        return sum(score)/(len(score)*ploidy), pairwise_dict, pairwise_list

    else:
        return 'NA', pairwise_dict, 'NA'

# data new

if __name__ == '__main__':

    description_text = 'Evaluation'
    epilog_text = 'Copyright Johan Willemsen May 2028 - May 2020'
    parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
    
    # The -i option should allow to input the allele matrix (so dosages vs genotypes)
    parser.add_argument('-i', type=str,default="./examples/gwd_output.haplotypes.txt", help='Input genotype matrix file')
    parser.add_argument('-j', type=str, default="../Example/gwd_hap.interval_0.long", help='Output prefix' )
    parser.add_argument('-ploidy', type=int, default=4, help='The ploidy level')
    parser.add_argument('-oldvsnew', action='store_true', help='If specified join the haplotypes')

    args = parser.parse_args()


    data_new = np.loadtxt(args.i, dtype='str')
    haplotypes_new = data_new[1:,2:].T

    if args.oldvsnew:
        haplotypes_old = np.loadtxt(args.j, dtype='str')
    else:
        data_old = np.loadtxt(args.j, dtype='str')
        haplotypes_old = data_old[1:,2:].T

    ploidy = args.ploidy

    scores = []

    for i in range(0, haplotypes_new.shape[0]-ploidy, ploidy):
        # print(i, i+ploidy)

        ind1 = haplotypes_new[i:i+ploidy]
        ind2 = haplotypes_old[i:i+ploidy]

        reconstruction_rate = get_number_of_correct_haps(ind1, ind2)
        pairwise_accuracy, pairwise_dict, pairwise_list = pair_wise_accuracy(ind1, ind2, ploidy=ploidy)
        no_switches, where_switch = get_switch_error(ind1, ind2, ploidy=ploidy)

        # print(reconstruction_rate, pairwise_accuracy, no_switches)
        scores += [[reconstruction_rate / ploidy, pairwise_accuracy, no_switches, no_switches/haplotypes_new.shape[1]]]

    scores = pd.DataFrame(scores, columns=['Reconstruction rate', 'Pairwise accuracy', 'no switches', 'switch error/snp'])

    print(scores.mean().to_dict())


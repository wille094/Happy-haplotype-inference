## About

- **Author:** Johan Willemsen
- **version:** v.1.0 (June 2020)
- **name:** Happy-inf 
- **Description:** Haplotype inference for diploids & polyploids.
- **reference**: [add] 

## Installation

In principle the installation via conda should work fine.

#### python 2.7
```
conda create -n happy-inf python=2.7.* 
conda activate happy-inf
pip install scipy numpy
```


#### python 3.7
```
conda create -n happy-inf_py37 python=3.7.* 
conda activate happy-inf_py37 
pip install scipy numpy

```

## Haplotype inference in polyploid crops

To obtain haplotypes of polyploid genotyping data we employ a stepwise approach:

1. **Stage 1:** Estimate pairwise phasings using the EM-algorithm. For n snps there are n(n-1)/2 pairwise combinations, which scales badly. In the input a column called "Block" needs to be specified, which allows to estimate pairwise phasings only for the markers within a block, significantly increasing the speed.

2. **Stage 2:** Iterative algorithm that glues pairwise phasings together to a full length haplotype. This is a simle seed-and-extend algorithm that starts with a specific phase and tries to elongate this by adding phases to other snps. 

When the EM-based phases are 100% correct, the joining algorithm is epected to find always the correct solution. In case of errors of the pairwise haplotypes we employ majority rule phasing and retain the most frequent solution.  *This algorithm is not deterministic so it will return slightly different solutions every time*

The number of steps to run this algorithm depends on the length of the haplotypes to be reconstructed. For a haplotype of 4 snps, there are 4! possible paths the seed-and extend algorithm can take. There is in these cases no point to increase the number of joining steps to more than 24. 
Default is 10 iterations (`-joining_steps`).  

The number of extra calculations for the joining step is as follows per ploidy level. 

|     dosage      	|     0    	|     1    	|     2     	|     3     	|     4     	|     5     	|     6     	|     7    	|     8    	|
|-----------------	|----------	|----------	|-----------	|-----------	|-----------	|-----------	|-----------	|----------	|----------	|
|     ploidy=2    	|     1    	|     2    	|     1     	|           	|           	|           	|           	|          	|          	|
|     ploidy=4    	|     1    	|     4    	|     6     	|     4     	|     1     	|           	|           	|          	|          	|
|     ploidy=6    	|     1    	|     6    	|     15    	|     20    	|     15    	|     6     	|     1     	|          	|          	|
|     ploidy=8    	|     1    	|     8    	|     28    	|     56    	|     70    	|     56    	|     28    	|     8    	|     1    	|

## Usage

With a single block containing 81 snps
```bash
python happy-inf.py -i examples/gwd.dat -ploidy 4 -join -o output/gwd_output
```

With several blocks containing 10, 15, 33, 12 and 11 markers.

```bash
python happy-inf.py -i examples/gwd.blocks.dat -ploidy 4 -join -o output/gwd_output.blocks
```

- Optionally the EM phasings can be filtered on genotype likelihood (`gl_tresh`), however this does not alwasys result in better haplotypes...
- The example dataset takes about 9 minutes on a high-end pc and recontructs haplotypes of size 81. 

```
usage: happy-inf.py [-h] [-i I] [-o O] [-ploidy PLOIDY] [-steps STEPS]
                     [-join] [-joining_steps JOINING_STEPS]
                     [-gl_tresh GL_TRESH]

Haplotype inference using EM-based heuristic phasing

optional arguments:
  -h, --help            show this help message and exit
  -i I                  Input genotype matrix file
  -o O                  Output prefix
  -ploidy PLOIDY        The ploidy level
  -steps STEPS          Number of iterations for EM
  -join                 If specified join the haplotypes
  -joining_steps JOINING_STEPS
                        Number of iterations for joining
  -gl_tresh GL_TRESH    Treshold for removing pairwise phasings. These wont be
                        considered for the joining algorithm

Copyright Johan Willemsen May 2018 - June 2020
```

## Input data

With `-i` and input file is specified that contains the allele dosage matrix, where columns represent samples and rows represent the markers. 

```
snp_ID	Block	gen1	gen2	gen3	gen4
marker1	block1	0	0	0	1
marker2	block1	2	0	0	1
marker3	block1	0	0	0	1
...
```

- Missing values are specified by NA, which mostly implies that these are imputed during the first stage of the haplotype inference (EM-stage). In the join stage these imputed haplotypes are used. 
- Blocks are specified by the Block column. 


## Output

We save the results of Stage 1 in two files:

1. **{output}.blk.dat**: Contains the 2-snp haps for each 2 SNP pair.
2. **{output}.freq.dat**: Contains the frequencies of the haplotypes over all individuals in each 2 SNP pair.

We save the output of Stage 2 in two files:

1. **{output}.haplotypes.dat**: Contains the full-length haplotypes for each interval
2. **{output}.stats.dat**: Contains the stats per block and individual. The "mismatches" describes the fraction of mismatches of the reconstructed solution to the number of underlying pairwise phasings. The ratio is the # most frequent / # of 2nd frequent solution, where the total of solutions equals the number of joining steps considered.



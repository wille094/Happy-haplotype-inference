from __future__ import division
from __future__ import print_function
import numpy as np
import scipy 
import os
import itertools
from scipy.special import gamma, gammaln
import math
from numpy import array, log, exp
import operator
import argparse
import time
import datetime
import random


np.seterr(divide='ignore', invalid='ignore')

def get_number_of_correct_haps(haps, selection):
    score = 0
    sel = list(selection[:])
    for hap in haps:
        if hap in sel:
            score += 1
            sel.pop(sel.index(hap))
    return score

def factorialln(n):
    if n == 1:
        return 0
    elif n == 0:
        return 0
    else: 
        return gammaln(n + 1)

class hwe:
    def __init__(self, ploidy, comp):
        self.ploidy = ploidy
        self.comp = comp

        self.v_index = np.vectorize(self.get_index)
        self.pre_cached_factorials = np.array([factorialln(i) for i in range(1000)])
    
    def get_index(self, x):
        return self.pre_cached_factorials[x]

    def multinomial_probabilities_array(self, n, observation_array):
        return np.exp( self.pre_cached_factorials[n] - self.v_index(observation_array).sum(axis=1))

    def calculate_pg(self, allele_frequencies, observation_array):
        genotype_coefficients = self.multinomial_probabilities_array(self.ploidy, self.comp) #checked results in the same
        second_part  = np.product( np.power(allele_frequencies,  self.comp ), axis=1)
        hwe = genotype_coefficients * second_part 
        return hwe

class em():

    def __init__(self, ploidy=4, alleles= ['0','1']):
        self.ploidy = ploidy
        # initial settings
        self.marker_alleles = [alleles, alleles]
        self.haps = list(itertools.product(*self.marker_alleles))
        self.haps_ids = range(len(self.haps))
        self.phasings = list(itertools.combinations_with_replacement(self.haps, ploidy))

        # Make haplotype indices list for complete list of haplotypes [35 x ploidy]
        self.comp = []      
        for i in itertools.combinations_with_replacement(self.haps_ids,ploidy):
            ph = tuple([i.count(j) for j in self.haps_ids])
            self.comp += [list(ph)]
        self.comp = np.array(self.comp)
        self.genotypes = [self.get_genotype(i) for i in self.phasings] # list with genotypes for phasings. Each genotype might occur twice. 
        self.haplotype_priors = self.haplotype_freq_priors()

        # Calculate P(G)
        self.hwe_ = hwe(ploidy, self.comp)
        self.expected_freq = self.hwe_.calculate_pg(self.haplotype_priors, self.comp)

    
    def get_genotype(self, ph):
        '''
        Computes the genotypes from all phasings.
        '''
        g = np.array(ph).T
        ge = [sorted(i.tolist()) for i in g]
        return tuple(ge)

    def haplotype_freq_priors(self, uniform=False):
        """
        Calculate the haplotype priors
        """
        if uniform is True:
            priors = [1/len(self.haps)] * len(self.haps)
        else:
            priors = np.random.random(size=len(self.haps))
            priors = priors / priors.sum()
        return priors


    def decomposition(self, obs=tuple([['0','0','0','A'], ['0','0','0','1']])):
        """
        Obs to potential phasings
        """

        miss_obs = ['NA']* self.ploidy
        # initialize missing values as all possible genotypes. Then imputation is automatically done. 
        if (obs[0] == miss_obs) or (obs[1] == miss_obs):
            # print(obs)
            decomposition_single = np.ones(len(self.genotypes))
            decomposition_single = decomposition_single / decomposition_single.sum()
            # print(decomposition_single)
        else:
            decomposition_single = np.zeros(len(self.genotypes))
            ind = [index for index, i in enumerate(self.genotypes) if i == obs]
            decomposition_single[ind] = 1
            decomposition_single = decomposition_single / decomposition_single.sum()
        # print(obs, decomposition_single)
        # for i in ind:
            # print(self.phasings[i])
        return decomposition_single
    
    def decomposition_all(self, observations):

        decomposed = []
        for index, i in enumerate(observations):
            single = self.decomposition(obs=i)
            decomposed += [single]

        self.decomposed_genotype_array = np.array(decomposed)

    def count_haplotypes_in_genotypes(self, counts):
        '''
        Simple count function that does compute the haplotype frequency based on genotype decomposition, i.e. 
        count[0] is 00|00|00|00, hence haplotype 00 has count of 4.  
        Used in M-step to compute re-estimated haplotype frequencies. 
        counts --  Counts of each phased genotype (35 possibilities for 2-locus)
        '''
        a = counts * self.comp
        allele_counts = a.sum(axis=0)

        allele_freq = allele_counts / allele_counts.sum()
        return allele_counts, allele_freq

    def em_algorithm(self, priors):
        
        posterior_probabilities = self.decomposed_genotype_array * priors
        updated_GL = posterior_probabilities / posterior_probabilities.sum(axis=1).reshape(self.decomposed_genotype_array.shape[0],1)
        
        # M-step Use the expected genotype frequencies to compute the haplotype frequencies. 
        counts_genotype = updated_GL.sum(axis=0, ) #shape=(self.decomposed_genotype_array.shape[0],1))
        counts_genotype.shape = (self.decomposed_genotype_array.shape[1],1)
        allele_counts, allele_frequencies = self.count_haplotypes_in_genotypes(counts_genotype)
        new_priors = self.hwe_.calculate_pg(allele_frequencies, self.comp)

        return new_priors, allele_frequencies, updated_GL, allele_counts    

    def iter_em(self, steps=50):

        new_priors, allele_frequencies_old, updated_GL, allele_counts = self.em_algorithm(self.expected_freq)
        allele_frequencies_new = True
        stop = np.abs(self.haplotype_priors - allele_frequencies_old).mean()
            
        step = 1
        while (stop > 0.000714286) and step < steps:
            
            new_priors, allele_frequencies_new, updated_GL, allele_counts = self.em_algorithm(new_priors)
            stop =  np.abs(allele_frequencies_new - allele_frequencies_old).mean()
            allele_frequencies_old = allele_frequencies_new
            step +=1

        posterior_probabilities = self.decomposed_genotype_array * new_priors

        self.allele_counts = allele_counts
        # Is this the stupid log-likelihood ?
        posterior_prob = np.product(posterior_probabilities.sum(axis=1))
        estimated_Gu = np.argmax(updated_GL, axis=1) # return the most probable assignment of haplotypes to individual
        
        # Get likelihood ratio
        LRs = []
        for i in updated_GL:
            temp_i = sorted(i)
            # print temp_i, temp_i[-1], temp_i[1], 
            try:
                lr = temp_i[-1] / (temp_i[-2] + 0.0000000000000000000000000000001)
                if lr > 50000:
                    lr = 50000
            except:
                lr = 50000

            LRs += [lr]


        self.estimated_freq = new_priors
        self.estimated_Gu = estimated_Gu
        if allele_frequencies_new is not True:
            self.allele_freq = allele_frequencies_new
        else:
            self.allele_freq = allele_frequencies_old


        # update total list

        self.GL_list += [updated_GL.max(axis=1)]
        self.estimated_freq_list += [self.estimated_freq]
        self.estimated_Gu_list += [self.estimated_Gu]
        self.allele_freq_list += [self.allele_freq]
        self.loglikelihood_list += [np.log10(posterior_prob)]
        self.allele_counts_list += [self.allele_counts]
        self.LR_list += [LRs]

    def select_best_run(self):
        """ 
        Select the best run of the EM algorithm
        """
        index = np.argmax(np.array(self.loglikelihood_list))
        self.estimated_freq = self.estimated_freq_list[index]
        # self.discard_indices = np.where((self.GL_list[index] < 0.0))[0]
        self.estimated_Gu = self.estimated_Gu_list[index]
        self.allele_freq = self.allele_freq_list[index]
        self.allele_counts = self.allele_counts_list[index]

        # Actual allele frequency based on estimated GLs, as the frequency estimate might be zero, still an unambigous individual can be inserted
        real_counts = []
        for i in self.estimated_Gu:
            real_counts += [[self.phasings[i].count(H) for H in self.haps]]
        
        self.real_counts = np.array(real_counts).sum(axis=0)
        self.real_freq = self.real_counts / self.real_counts.sum()

        self.GLs = self.GL_list[index]
        self.LRs = self.LR_list[index]

    def create_haplotypes(self, indices_modus=None):

        if indices_modus == None:
            indices_modus = self.estimated_Gu

        test = [['|'.join([''.join(i) for i in j])] for j in self.phasings]
        phased_genotypes = []
        phased_real = []
        phased_indices = []

        for i in indices_modus:
            if i != 'NA':
                # print i
                phased_genotypes += test[int(i)]
                phased_real += [np.array(self.phasings[int(i)]).T.tolist()]
                phased_indices += [str(i)]
            else:
                phased_genotypes += ['NA']
                phased_real += ['NA']
                phased_indices += ['NA']
        self.phased_genotypes = phased_genotypes
        self.phased_real = phased_real
        self.phased_indices = phased_indices

        # input for joining

    def wrap_em(self, iterations = 10):
        
        self.estimated_Gu_list = []
        self.loglikelihood_list = []
        self.estimated_freq_list = []
        self.allele_freq_list = []
        self.GL_list = []
        self.allele_counts_list = []
        self.LR_list = []

        for index in range(iterations):
            self.iter_em()

        self.select_best_run()


class join():
    def __init__(self, results=None, verbose=False, ploidy=4):
        self.results = results
        self.verbose = verbose
        self.ploidy = ploidy
        self.extens = list(itertools.permutations(range(self.ploidy), self.ploidy))

    def get_seed(self, phasings, alleles):

        snps = [k for k in alleles]
        # print(snps)
        random.shuffle(snps)
        seed = tuple(sorted([snps[0], snps[1]])) # tmp
        
        if self.verbose:
            print("Seed :", seed)

        while seed not in phasings:
            random.shuffle(snps)
            seed = tuple(sorted([snps[0], snps[1]])) # tmp
        # remove the snp from the list. 
        snps.remove(seed[0])
        snps.remove(seed[1])

        # initialize solution
        solution =  {
            seed[0]: tuple(phasings[seed][0]), 
            seed[1]: tuple(phasings[seed][1])
        }
        return seed, solution, snps

    def seed_and_extend(self, phasings, alleles, no_snps):
        
        seed, solution, snps = self.get_seed(phasings, alleles)

        if self.verbose:
            print("Seed solution: ")
            for i in solution:
                print(i, solution[i])

        if self.verbose:
            print("Run extension process")
        # Run extension process per snp
        while snps != []:
            next_snp = random.choice(snps)
            snps.remove(next_snp)
            solution = self.fetch_extension(next_snp, alleles, phasings, solution)
        
        self.score = self.estimate_fitness(solution, phasings)
        
        if self.verbose:
            for i in solution:
                print(i, solution[i])
            print("Number of mismatches Final solution", self.estimate_fitness(solution, phasings)    )

        self.solution = solution

    def fetch_extension(self, x, alleles, phasings, solution):
        list_solutions = self.get_possible_extensions(x, alleles, solution)

        # get mismatches for all extensions
        scores = []
        for test_solution in list_solutions:

            mismatches = self.return_mismatches(test_solution, phasings, x)
            scores += [mismatches]

        # get best extensions
        ind  = scores.index(min(scores))
        return list_solutions[ind]


    def get_possible_extensions(self, x, alleles, solution):
        """
        Defines the 24 possible extensions given the snp, unordered alles and the current solution
        Or 720 in case of ploidy equals 6
        """
        
        list_solutions = []
        
         # all orders of a thing. self.extens, because we have bi-allelic markers we could pre-compute these , not implemented at this pint.  
        extensions_phase = list(set([tuple([alleles[x][j] for j in i]) for i in  self.extens]))
        # print(extensions_phase) 

        for index, possible_extension in enumerate(extensions_phase): #TODO: There are unique solutions in here.
            test_solution = solution.copy()
            # print("extension :", index)
            test_solution[x] = possible_extension
            list_solutions += [test_solution]

        #TODO: GET RID OF NON-UNIQUE SOLUTIONS
        return list_solutions

    def return_mismatches(self, test_solution, phasings, x):
        """
        Match theoretical hap to estimated phasings for snp x
        Return number of mismatches for this solution. 
        """

        # get extensions to the next snp   
 
        snps_left = [k for k in test_solution]
        snps_left.remove(x)
        combis = [tuple(sorted(i)) for i in itertools.product(snps_left, [x])]
        combis = [i for i in combis if i in phasings]

        mismatches = 0

        for y in combis:

            solution_hap = self.solution_to_hap(y, test_solution)
            test_hap = self.phase_to_hap(y, phasings)
            mismatch =  self.ploidy - get_number_of_correct_haps(test_hap, solution_hap)
            mismatches += mismatch
        # print("Number mismatches for solution :", mismatches)
        return mismatches

    def estimate_fitness(self, solution, phasings):
        """
        Get mismatches for all phasings
        """
        mismatches = 0

        # print("###########################3new ittttt")
        for y in phasings:

            solution_hap = self.solution_to_hap(y, solution)
            test_hap = self.phase_to_hap(y, phasings)
            mismatch =  self.ploidy - get_number_of_correct_haps(test_hap, solution_hap)

            # print(solution_hap)
            # print(test_hap)
            # print(y, mismatch)
            # if mismatch > 0:
            #     print(mismatch)
            #     print(solution_hap)
            #     print(test_hap)
            mismatches += mismatch

        # print("No mismatches", mismatches)
        return mismatches

    def phase_to_hap(self, x, phasings):
        """
        Convert phase dict to haplotypes
        """
        
        snp1, snp2 = phasings[x][0], phasings[x][1]
        test_hap = np.array([snp1, snp2]).T.tolist()
        return test_hap

    def solution_to_hap(self, x, solution):
        """
        Convert solution to haplotypes
        """
        solution_hap = np.array([solution[x[0]], solution[x[1]]]).T.tolist()
        return solution_hap

    def get_phases(self, solution):
        """
        Get potential phases (0,1) for the solution
        """

        combis = list(itertools.combinations(solution.keys(), 2))

        phases = {}
        for i in combis:
            phases[i] = [solution[i[0]], solution[i[0]]]
        return phases

    def iter_join(self, phasings, allele_matrix, no_snps, iterations=10):
        """
        Run the joining algorithm 100 times. 
        """

        k = 0

        solutions = []
        scores = []

        while k < iterations:
            # h_join = join()
            self.seed_and_extend(phasings, allele_matrix, no_snps)
            k += 1

            solutions += [self.solution]
            scores += [self.score]

        # get best solution
        keys = allele_matrix.keys()

        indices = []
        unique_phasings = []
        unique_phasings_count = [] # majority rule phasing
        unique_phasing_scores = []

        # define which solutio is the best. 
        for index, solution in enumerate(solutions):

            haplotypes = np.array([solution[j] for j in keys]).T.tolist()
            
            # get the scores for a solution. if score equals ploidy then it is not unique. 
            scores_one_solution = [get_number_of_correct_haps(haplotypes, i) for i in unique_phasings]
            if self.ploidy not in scores_one_solution:
                unique_phasings += [haplotypes]
                unique_phasings_count += [1]
                indices += [index]
                unique_phasing_scores += [scores[index]]
            else:
                scores_ind = scores_one_solution.index(self.ploidy)
                unique_phasings_count[scores_ind] += 1

        # calculate Phase ratio
        if len(indices) > 1:
            ratio =  sorted(unique_phasings_count)[-1] / sorted(unique_phasings_count)[-2]
        else:
            ratio = 1e5

        best_index = unique_phasings_count.index(max(unique_phasings_count))
        best_solution = solutions[best_index]

        # Save best solution
        self.solution = best_solution
        self.score = unique_phasing_scores[best_index]
        self.ratio = ratio
    
    def include_na(self, block):
        """
        Post hoc include NA dosages in the haplotypes. 
        """
        for i in block:
            if i not in self.solution:
                self.solution[i] = tuple(['NA'] * self.ploidy)
        

import os.path

class logger:
    def __init__(self, prefix, block_name='block1', genotype_names=None, marker_names=None, ploidy=4):
        self.block_name = block_name
        self.genotype_names = genotype_names
        self.marker_names = marker_names
        # print(self.marker_names)
        self.prefix = prefix
        
        # blk file
        if os.path.isfile(prefix + '.blk.dat') == False:
            header = ['Block', 'SNP1', 'SNP2'] + self.genotype_names.tolist()
            header = '\t'.join(header)

            self.file_blk = open(prefix + '.blk.dat', 'a')
            self.file_blk.write(header + '\n')
        else:
            self.file_blk = open(prefix + '.blk.dat', 'a')
        
        # frequency file
        if os.path.isfile(prefix + '.freq.dat') == False:
            header = ['Block', 'SNP1', 'SNP2', '00', '01', '10', '11']
            header = '\t'.join(header)
            self.file_freq = open(prefix + '.freq.dat', 'a')
            self.file_freq.write(header + '\n')
        else:
            self.file_freq = open(prefix + '.freq.dat', 'a')

        # open haplotype file
        if  os.path.isfile(prefix + '.haplotypes.dat') == False:

            header = ['Block', 'SNP_ID'] + ['\t'.join(['%s_%i' %(i, j) for j in range(1, ploidy+1)]) for i in self.genotype_names]
            header = '\t'.join(header)
        
            self.file_join = open(prefix + '.haplotypes.dat', 'a')
            self.file_join.write(header + '\n')
        else:
            self.file_join = open(prefix + '.haplotypes.dat', 'a')
        
        # open stats file
        if os.path.isfile(prefix + '.stat.dat') == False:
            header_stat = ['Block', 'type'] + self.genotype_names.tolist()
            
            self.file_stat = open(prefix + '.stat.dat', 'a')
            self.file_stat.write('\t'.join(header_stat) + '\n')
        else:
            self.file_stat = open(prefix + '.stat.dat', 'a')

    def write_em(self, results):

        # [h.phased_real, h.real_freq, h.phased_genotypes, h.phased_indices] 

        for y in results:

            key = [self.block_name, str(y[0]), str(y[1])]
            hapfreq = [str(round(i, 2)) for i in results[y][1]]
            gen = results[y][2]
            haps = [''.join(i) for i in results[y][4]]
            # print(haps)

            self.file_blk.write('\t'.join(key + gen) + '\n')
            self.file_freq.write('\t'.join(key + hapfreq) + '\n')

    def write_join(self, results, ploidy=4):

        # header = ['Block', 'SNP_ID'] + ['\t'.join(['%s_%i' %(i, j) for j in range(1, ploidy+1)]) for i in self.genotype_names]
        # header = '\t'.join(header)
        # self.file_join.write(header + '\n')

        snps = sorted(results[0][0].keys())
        # print(snps)

        for snp_id in snps:
            
            row = ['\t'.join(results[ind][0][snp_id]) for ind in results]
            row = '\t'.join([self.block_name, str(self.marker_names[snp_id])] + row)
            self.file_join.write(row + '\n')

        scores = [str(results[ind][1]) for ind in results]
        ratios = [str(results[ind][2]) for ind in results]

        self.file_stat.write('\t'.join([self.block_name, 'mismatch'] + scores) + '\n')
        self.file_stat.write('\t'.join([self.block_name, 'ratio'] + ratios) + '\n')


    def close(self):

        self.file_join.close()
        self.file_blk.close()
        self.file_freq.close()

def convert_allele_dosage(ploidy, g):
    # convert allele dosage (1) to [1, 0, 0, 0,] or [NA x 4] in case of missing data
    # So allele dosage is converted to list of allestates. 
    snp1 = ['0'] * ploidy
    try:
        for j in range(int(g)):
            snp1[j] = '1'
        return sorted(snp1)
    except:
        return ['NA'] * ploidy

def convert_block_to_alleles(block, ploidy=4):
    converted = []

    for row in block:
        converted += [tuple([convert_allele_dosage(ploidy, row[0]), convert_allele_dosage(ploidy, row[1])])]

    return converted

def fetch_data_join(result, ind, dosages, gl_tresh=1.0, block=None):
    """
    Fetch alleles (computed by em) and phasings (computed by em)
    for joining step. 

    Here we do preprocessing of the pairwise phasings
    i.e. lower than x lr is deleted, etc. 
    """
    # print(dosages)
    which_na = np.where(dosages == 'NA')[0].tolist()
    which_na = np.array(block, dtype=int)[which_na].tolist()

    allele_matrix = {}
    phasings = {}

    for y in result:
        if (y[0] not in which_na) and (y[1] not in which_na):
            # determine GL:
            gl = result[y][5][ind]
            # print(gl)
            if gl >= gl_tresh:
                # print(gl)

                allele_matrix[y[0]] = result[y][0][ind][0]
                allele_matrix[y[1]]= result[y][0][ind][1]
                phasings[y] = result[y][0][ind]
    
    no_snps = len(allele_matrix.keys())
    return allele_matrix, phasings, no_snps


def main():
    description_text = 'Haplotype inference using EM-based heuristic phasing'
    epilog_text = 'Copyright Johan Willemsen May 2018 - May 2020'
    parser = argparse.ArgumentParser(description=description_text, epilog=epilog_text)
    
    # The -i option should allow to input the allele matrix (so dosages vs genotypes)
    parser.add_argument('-i', type=str,default='-', help='Input genotype matrix file', required=True)
    parser.add_argument('-o', type=str, default='-', help='Output prefix', required=True)
    parser.add_argument('-ploidy', type=int, default=4, help='The ploidy level', required=True)

    parser.add_argument('-steps', type=int, default=10, help='Number of iterations for EM')
    parser.add_argument('-join', action='store_true', help='If specified join the haplotypes')
    parser.add_argument('-joining_steps', type=int, default=10, help='Number of iterations for joining')
    parser.add_argument('-gl_tresh', type=float, default=0, help='Treshold for removing pairwise phasings. These wont be considered for the joining algorithm')


    args = parser.parse_args()

    # argumnets
    ploidy = args.ploidy
    
    # load in data new format (markers on rows and genotypes on columns)
    data = np.loadtxt(args.i, dtype='str')
    genotype_matrix = data[1:, 2:]
    marker_names = data[1:,0]
    interval_data = data[1:,1]
    genotype_names = data[0,2:]
    no_markers, no_genotypes = genotype_matrix.shape

    # block names + intervals
    names = np.unique(interval_data).tolist()
    intervals = [np.where(interval_data == i)[0].tolist() for i in names]
    # Then perform the analysis per block
    for index_block, block in enumerate(intervals):
        print(datetime.datetime.now().strftime("%H:%M:%S\t"), "Haplotype reconstruction in %s containing %i markers"% (names[index_block], len(intervals[index_block])))
        # employ the pairwise phasing. 
        h = em(ploidy=ploidy)

        results_pairwise = {}
        for (a, b) in itertools.combinations(block, 2):

            converted = convert_block_to_alleles(genotype_matrix[[a,b]].T, ploidy=ploidy)
            h.decomposition_all(converted)
            h.wrap_em(iterations=args.steps)
            h.create_haplotypes()

            # print(h.LRs)

            results_pairwise[(a,b)] = [h.phased_real, h.real_freq, h.phased_genotypes, h.phased_indices, h.haps, h.GLs, h.LRs]  

        # Save this information to log file. 
        log = logger(args.o, block_name=names[index_block], genotype_names=genotype_names, marker_names=marker_names, ploidy=ploidy)
        log.write_em(results_pairwise)

        print(datetime.datetime.now().strftime("%H:%M:%S\t"), "Stage 1 complete" )
        ## JOIN EM PHASES INTO ONE HAPLOTYPE
        ## LOOP OVER EVERY INDIVIDUAL

        if args.join:
            # start joining process
            results_join = {}

            # # run joinnig step over all genotypes
            for gen in range(no_genotypes):
                allele_matrix, phasings, no_snps = fetch_data_join(results_pairwise, gen, genotype_matrix[block, gen], gl_tresh=args.gl_tresh, block=block)
    
                if allele_matrix == {}:
                    # no snps, only missing data. 
                    h_join = join(ploidy=ploidy)
                    h_join.solution = {}
                    h_join.include_na(block)
                    h_join.score = np.nan
                    h_join.ratio = np.nan
                else:
                    h_join = join(ploidy=ploidy)
                    h_join.iter_join(phasings, allele_matrix, no_snps, iterations=args.joining_steps)                   
                    h_join.include_na(block) # check if solution is complete
                
                # error with the scoring if only NAs are present in a block
                try:
                    test_score1 = h_join.score / len(phasings)
                except:
                    test_score1 = 'NA'

                results_join[gen] = [h_join.solution, test_score1, h_join.ratio]

            log.write_join(results_join, ploidy=ploidy)
            log.close()
        print(datetime.datetime.now().strftime("%H:%M:%S\t"), "Stage 2 complete" )

if __name__ == '__main__':
    main()